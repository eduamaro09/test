﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;

using Televisa.Trebol.Bussiness;
using Televisa.Trebol.Business.Entities;
using Televisa.Trebol.Bussiness.Validators;
using Televisa.Trebol.Contracts;
using Televisa.Trebol.Contracts.DataAccess;

using Televisa.Trebol.Api.Models;
using Televisa.Trebol.Api.Support.ErrorManagement;

using Microsoft.AspNet.Identity;
using System.Web.Http.ModelBinding;
using AutoMapper;
using System.Web.Http.Description;
using Televisa.Trebol.Api.Support.Providers;

namespace Televisa.Trebol.Api.Controllers
{
    /// <summary>
    /// Infomación del historial de transacciones del jugador
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class PlayerHistoryController : ApiController
    {
        private IPlayerManager _playerManager;
        private IPlayerHistoryDao _playerHistoryDao;
        private IPlayerHistoryManager _playerHistoryManager; 
        private IAuthenticationManager Autentication => Request.GetOwinContext().Authentication;
        IMapper _mapper;

        public PlayerHistoryController(IPlayerManager playerManager, IPlayerHistoryManager playerHistoryManager, IPlayerHistoryDao playerHistoryDao, IMapper mapper)
        {
            _playerHistoryManager = playerHistoryManager;
            _playerHistoryDao = playerHistoryDao;
            _playerManager = playerManager;
            _mapper = mapper;

        }


        /// <summary>
        /// Obtiene el historial del jugador de ajustes por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("adjustment")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetHistory([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {
                long txId = 0;

                if(!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }


#if DEBUG
                var history = await _playerHistoryManager.GetAdjustment(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var lst = new List<AdjustmentDetailsDTO>();

                var history = new AdjustmentDetailsDTO
                {
                    Amount = 10.0M,
                    DT = DateTime.Now,
                    TxId = 1,
                    AdminUser = "admin", 
                    IntName = "IntName", 
                    PlayerId  = 1,
                    Reason = "Descripción"
                };


#endif

                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<AdjustmentDetailsDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial de apuestas canceladas del jugador por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("betcancelticket")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetBetCancelTicket([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {

                long txId = 0;

                if (!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }
#if DEBUG
                var history = await _playerHistoryManager.GetBetCancelTicket(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var lst = new List<BetCancelTicketDTO>();

                var history = new BetCancelTicketDTO
                {
                    Amount = 10.0M,
                    DT = DateTime.Now,
                    TxId = 1,
                    AdminUser = "admin", 
                    IntName = "IntName", 
                    PlayerId  = 1,
                    LinkedTxId = 2, 
                    BetTicket = new BetTicketDTO {
                            Amount = 1.0M,
                            BetAmount = 1.0M,
                            BetState = "Estado",
                            BetTicketDetailGG = null, 
                            BetTicketDetails = null, 
                            BetType = BetType.GanaGol, 
                            DrawNumber = 1, 
                            DT = DateTime.Now, 
                            DTExported = DateTime.Now,
                            DTResponse = DateTime.Now,
                            NumberOfDraws = 10, 
                            PlayerId = 1, 
                            TxeTktSerial = 1, 
                            TxId  = 1, 
                    }
                };

                

#endif

                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<BetCancelTicketDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial de apuestas rechazadas del jugador por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("rejectticket")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetBetRejectTicket([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {
                long txId = 0;

                if (!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var history = await _playerHistoryManager.GetBetRejectTicket(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var lst = new List<BetErrorTicketDTO>();

                var history = new BetErrorTicketDTO
                {
                    Amount = 10.0M,
                    DT = DateTime.Now,
                    TxId = 1,
                    PlayerId  = 1,
                    ErrorCode = 1, 
                    ErrorString = "Error", 
                    LinkedTxId = 1, 
                    BetTicket = new BetTicketDTO
                    {
                        Amount = 1.0M,
                        BetAmount = 1.0M,
                        BetState = "Estado",
                        BetTicketDetailGG = null,
                        BetTicketDetails = null,
                        BetType = BetType.GanaGol,
                        DrawNumber = 1,
                        DT = DateTime.Now,
                        DTExported = DateTime.Now,
                        DTResponse = DateTime.Now,
                        NumberOfDraws = 10,
                        PlayerId = 1,
                        TxeTktSerial = 1,
                        TxId = 1,
                    }
                };

                

#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<BetErrorTicketDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial de apuestas del jugador por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("betticket")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetBetTicket([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {
                long txId = 0;

                if (!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var history = await _playerHistoryManager.GetBetTicket(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

                var history = new BetTicketDTO
                {
                    Amount = 1.0M, 
                    BetAmount = 1.0M,
                    BetState = "Estado",
                    BetTicketDetailGG = null, 
                    BetTicketDetails = null, 
                    BetType = BetType.GanaGol, 
                    DrawNumber = 1, 
                    DT = DateTime.Now, 
                    DTExported = DateTime.Now, 
                    DTResponse = DateTime.Now, 
                    NumberOfDraws = 10, 
                    PlayerId = 1, 
                    TxeTktSerial = 1, 
                    TxId = 1, 
                };

#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<BetTicketDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial de apuestas de BlackHawk del jugador por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("blackhawkcredited")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetBlackHawkCredited([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {
                long txId = 0;

                if (!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var history = await _playerHistoryManager.GetBlackHawkCredited(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var lst = new BlackHawkDetailsDTO {
                    Amount = 10.0M,
                    DT = DateTime.Now,
                    TxId = 1,
                    IntName = "IntName",
                    PlayerId = 1,
                    InventoryId = "1", 
                };


                var history = lst;

#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<BlackHawkDetailsDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial de cashout del jugador por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("cashout")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetCashOutDetails([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {
                long txId = 0;

                if (!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }


#if DEBUG
                var history = await _playerHistoryManager.GetCashOutDetails(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var history = new CashOutDetailsDTO
                {
                    Amount = 10.0M,
                    DT = DateTime.Now,
                    TxId = 1,
                    AdminUser = "admin", 
                    IntName = "IntName", 
                    PlayerId  = 1,
                    DTEnd = DateTime.Now,
                    DTStart = DateTime.Now,
                    EndID32  = "Final", 
                    Fee = 1.0M,
                    FeeReason = "Comentarios", 
                    StartID32= "Inicio"
                };


#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<CashOutDetailsDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial de cashout reversadas del jugador por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("cashooutreversal")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetCashOutReversalDetails([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {

                long txId = 0;

                if (!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var history = await _playerHistoryManager.GetCashOutReversalDetails(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

                var history = new CashOutReversalDetailsDTO
                {
                    Amount = 10.0M,
                    DT = DateTime.Now,
                    TxId = 1,
                    IntName = "IntName",
                    PlayerId = 1,
                    ErrorString= "Error", 
                    Fee= 1.0M,
                    FeeReason = "Comentario"
                };


#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<CashOutReversalDetailsDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial de transacciones por tarjeta de crédito del jugador por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("creditcard")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetCreditCardDetails([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {
                long txId = 0;

                if (!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var history = await _playerHistoryManager.GetCreditCardDetails(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

                var history = new CreditCardDetailsDTO
                {
                    Amount = 10.0M,
                    DT = DateTime.Now,
                    TxId = 1,
                    IntName = "IntName", 
                    PlayerId  = 1,
                    Authorization = "Autorizó", 
                    CardType = "Tipo", 
                    CCLast4Digits = "7890", 
                    ErrorString = "Error", 
                    Fee = 1.0M,
                    FeeReason = "Comentario", 
                    FolioNr = "Folio", 
                    State = "Estado"
                };


#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<CreditCardDetailsDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial de transacciones rechazadas por tarjeta de crédito del jugador por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("creditcardreversal")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetCreditCardReversalDetails([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {
                long txId = 0;

                if (!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var history = await _playerHistoryManager.GetCreditCardReversalDetails(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

                var history = new CashOutReversalDetailsDTO
                {
                    Amount = 10.0M,
                    DT = DateTime.Now,
                    TxId = 1,
                    IntName = "IntName",
                    PlayerId = 1,
                    ErrorString = "Error",
                    Fee = 1.0M,
                    FeeReason = "Comentario",
                };


#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<CreditCardDetailsDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial de transacciones de dinero del jugador por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("dinero")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetDineroDetails([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {
                long txId = 0;

                if (!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var history = await _playerHistoryManager.GetDineroDetails(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

                var history = new DineroDetailsDTO
                {
                    Amount = 10.0M,
                    DT = DateTime.Now,
                    TxId = 1,
                    IntName = "IntName",
                    PlayerId = 1,
                    ErrorString = "Error",
                    Fee = 1.0M,
                    FeeReason = "Comentario",
                    BarcodeDigits = "Barcode", 
                    BarcodeImageURL  = "http://algo.com",
                    DTPayment = DateTime.Now, 
                    FolioNr = "Folio", 
                    PaymentTxId = 1,
                    ProviderName = "Proveedor",
                    State = "Estado", 
                    VoucherURL = "http://algo.com",

                };


#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<DineroDetailsDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial de transacciones del jugador por medio de un id de HistoryRequest
        /// </summary>
        [Authorize]
        [ActionName("history")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetPlayerHistory([NakedBody]string jsonModel/*HistoryRequestDTO*/)
        {
            HttpResponseMessage response;

            try
            {
                HistoryRequestDTO model = jsonModel.ToString().Decrypt().JsonParse<HistoryRequestDTO>();


                ///TODO: Falta poner un validador del model Aqui

#if DEBUG

                var player = await _playerManager.GetPlayer(User.Identity.Name);
                var userId = player.PlayerId; //int.Parse(User.Identity.GetUserId());
                
                model.Locale = Locale.EsMx;
                var history = await _playerHistoryManager.GetPlayerHistory(userId, model.Locale, model.Skip, model.Take);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var lst = new List<TransactionDTO>();

                lst.Add(new TransactionDTO
                {
                    Amount = 10.0M,
                    BetState = "Demo",
                    DT = DateTime.Now,
                    JournalNumber = 1,
                    MoneyTxType = Locale.EsMx.ToString(),
                    TxDetails = "Detalle Demo",
                    TxId = 1,
                    TxType = Guid.NewGuid().ToString(),
                });

                var history = new PlayerHistoryPageDTO
                {
                    TotalCount = 1,
                    Transactions = lst.ToArray(),
                };

#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<PlayerHistoryPageDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial de apuestas ganadas del jugador por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("win")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetWinDetails([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {

                long txId = 0;

                if (!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var history = await _playerHistoryManager.GetWinDetails(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

                var history = new WinDetailsDTO
                {
                    Amount = 10.0M,
                    DT = DateTime.Now,
                    TxId = 1,
                    PlayerId = 1,
                    BetTicket = null, 
                    BetTxId = 1, 
                    CountyTax = 1.0M,
                    FederalTax = 1.0M, 
                    StateTax = 1.0M, 
                    WinMoneyDetails = null, 
                };


#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<WinDetailsDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial de apuestas ganadasno oficiales del jugador por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("winunofficial")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetWinUnofficialDetails([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {

                long txId = 0;

                if (!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var history = await _playerHistoryManager.GetWinUnofficialDetails(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

                var history = new WinUnofficialDetailDTO
                {
                    Amount = 10.0M,
                    DT = DateTime.Now,
                    TxId = 1,
                    PlayerId = 1,
                    BetNr = 1, 
                    IntName = "Nombre"
                };


#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<WinUnofficialDetailDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial transferencias bancarias del jugador por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("wiretransfer")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetWireTransferDetails([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {

                long txId = 0;

                if (!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }
#if DEBUG
                var history = await _playerHistoryManager.GetWireTransferDetails(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

                var history = new WireTransferDetailDTO
                {
                    Amount = 10.0M,
                    DT = DateTime.Now,
                    TxId = 1,
                    PlayerId = 1,
                    IntName = "Nombre", 
                    AccountNr = "1234567890", 
                    Authorization = "Autorización", 
                    ErrorString  ="Error", 
                    Fee = 1.0M, 
                    FeeReason = "Comentario", 
                    FolioNr = "Folio 1234568",
                    State = "Estado"
                };


#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<WireTransferDetailDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial transferencias bancarias reversadas del jugador por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("wiretransferreversal")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetWireTransferReversalDetails([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {
                long txId = 0;

                if (!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var history = await _playerHistoryManager.GetWireTransferReversalDetails(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

                var history = new WireTransferDetailDTO
                {
                    Amount = 10.0M,
                    DT = DateTime.Now,
                    TxId = 1,
                    PlayerId = 1,
                    IntName = "Nombre", 
                    AccountNr = "1234567890", 
                    Authorization = "Autorización", 
                    ErrorString  ="Error", 
                    Fee = 1.0M, 
                    FeeReason = "Comentario", 
                    FolioNr = "Folio 1234568",
                    State = "Estado"
                };


#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<WireTransferDetailDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el historial registro de copa del mundo del jugador por medio de un id de transacción
        /// </summary>
        [Authorize]
        [ActionName("worldcupregister")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetWorlCupRegisterDetails([NakedBody]string jsonModel/*long*/)
        {
            HttpResponseMessage response;

            try
            {
                long txId = 0;

                if (!long.TryParse(jsonModel.Decrypt(), out txId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }
#if DEBUG
                var history = await _playerHistoryManager.GetWorlCupRegisterDetails(txId);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

                var history = new WorlCupRegisterDetailDTO
                {
                    Amount = 10.0M,
                    DT = DateTime.Now,
                    TxId = 1,
                    PlayerId = 1,
                    IntName = "Nombre",
                    Bets =  12, 
                    TXESerial = 1
                };


#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<WorlCupRegisterDetailDTO>(history));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }
    }
}

﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Televisa.Trebol.Api.Models;

namespace Televisa.Trebol.Api.Support.CustomValidators
{
    public class LoginValidator : AbstractValidator<LoginDTO>
    {
        public LoginValidator()
        {
            RuleFor(l => l.Username).NotEmpty();
            RuleFor(l => l.Username).Length(1, 250);
            RuleFor(l => l.Username).EmailAddress();
            RuleFor(l => l.Password).NotEmpty();
            RuleFor(l => l.Password).Length(1, 16);
        }
    }
}
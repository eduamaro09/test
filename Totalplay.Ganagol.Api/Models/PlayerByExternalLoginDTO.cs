﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Televisa.Trebol.Api.Models
{
    public class PlayerByExternalLoginDTO
    {
        public int LoginProvider { get; set; }
        public string ExternalUserId { get; set; }  
    }
}
﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Televisa.Trebol.Api.Models;

namespace Televisa.Trebol.Api.Support.CustomValidators
{
    public class DrawResultsValidator : AbstractValidator<DrawResultsDTO>
    {
        public DrawResultsValidator()
        {
            RuleFor(l => l.DrawNumber).GreaterThan(0);
            RuleFor(l => l.GameId).GreaterThan(0);
        }
    }

}
﻿using System;
using System.Web;
using System.Web.Mvc;

namespace Televisa.Trebol.Api
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
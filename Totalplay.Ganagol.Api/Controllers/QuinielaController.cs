﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;

using Televisa.Trebol.Business.Entities;
using Televisa.Trebol.Bussiness.Validators;
using Televisa.Trebol.Bussiness.Validators.Requests;
using Televisa.Trebol.Contracts;
using Televisa.Trebol.Contracts.DataAccess;

using Televisa.Trebol.Api.Models;
using Televisa.Trebol.Api.Support.CustomValidators;
using Televisa.Trebol.Api.Support.ErrorManagement;
using FluentValidation.Results;
using Televisa.Trebol.Entities.DTOs;
using AutoMapper;
using System.Web.Http.Description;
using Televisa.Trebol.Api.Support.Providers;

namespace Televisa.Trebol.Api.Controllers
{
    /// <summary>
    /// Quiniela es para Ganagol 
    /// --> 
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class QuinielaController : ApiController
    {
        private IQuinielaDao _quinielaDao;
        private IQuinielaManager _manager;
        IMapper _mapper;

        private IAuthenticationManager Autentication => Request.GetOwinContext().Authentication;

        public QuinielaController(IQuinielaManager manager, IQuinielaDao quinielaDao, IMapper mapper) {
            _manager = manager;
            _quinielaDao = quinielaDao;
            _mapper = mapper;
        }

        /// <summary>
        /// Quiniela actual 
        /// </summary>
        [Authorize]
        [ActionName("current")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetCurrent()
        {
            HttpResponseMessage response;

            try
            {


#if DEBUG
                var quiniela = await _manager.GetCurrent();
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var lst = new List<QuinielaDTO>();

                var quiniela = new QuinielaDTO
                {
                    Draw = null,
                    Matches = null, 
                };
#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<QuinielaDTO>(quiniela));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Se obtiene los resultados al momento
        /// </summary>
        [Authorize]
        [ActionName("minuteresults")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetMinuteResults()
        {
            HttpResponseMessage response;

            try
            {


#if DEBUG
                var minute = await _manager.GetMinuteResults();
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var lst = new List<MinuteResultDTO>();

                var item = new MinuteResultDTO
                {
                    Away = "Fuera", 
                    AwayScore = 1, 
                    AwayTeamId= 1, 
                    Home = "Casa", 
                    HomeScore = 1, 
                    HomeTeamId = 1, 
                    League = "Liga", 
                    MatchDate = DateTime.Now,
                    MatchTime = DateTime.Now, 
                    Number = 1
                };

                lst.Add(item);
                var minute = lst;
#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<IEnumerable<MinuteResultDTO>>(minute));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene una quiniela por medio del número de sorteo
        /// </summary>
        [Authorize]
        [ActionName("quiniela")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetQuiniela([NakedBody]string jsonModel/*int*/)
        {
            HttpResponseMessage response;

            try
            {
                int drawNumber = 0;

                if (!int.TryParse(jsonModel.Decrypt(), out drawNumber))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }



#if DEBUG
                var quiniela = await _manager.GetQuiniela(drawNumber);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var lst = new List<MinuteResultDTO>();

                var item = new QuinielaResultDTO
                {
                    DrawNumber = 1, 
                    Goals = 2, 
                    MatchResults = null
                };
                //lst.Add(item);

                var quiniela = item;

#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<QuinielaDTO>(quiniela));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtener el ultimo resultado de los juegos 
        /// </summary>
        [Authorize]
        [ActionName("quinielaresult")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetQuinielaResults([NakedBody]string jsonModel/*int*/)
        {
            HttpResponseMessage response;

            try
            {

                int drawNumber = 0;

                if (!int.TryParse(jsonModel.Decrypt(), out drawNumber))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var quiniela = await _manager.GetQuinielaResults(drawNumber);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var lst = new List<MinuteResultDTO>();

                var item = new QuinielaResultDTO
                {
                    DrawNumber = 1, 
                    Goals = 2, 
                    MatchResults = null
                };
                //lst.Add(item);

                var quiniela = item;

#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<QuinielaResultDTO>(quiniela));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }


        /// <summary>
        /// Obtener las posibilidades
        /// </summary>
        [Authorize]
        [ActionName("odds")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetOdds([NakedBody]string jsonModel/*int*/)
        {
            HttpResponseMessage response;

            try
            {

                int drawNumber = 0;

                if (!int.TryParse(jsonModel.Decrypt(), out drawNumber))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var quiniela = await _manager.GetOdds(drawNumber);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var lst = new List<MinuteResultDTO>();

                var item = new QuinielaResultDTO
                {
                    DrawNumber = 1, 
                    Goals = 2, 
                    MatchResults = null
                };
                //lst.Add(item);

                var quiniela = item;

#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<IEnumerable<GanaGolOddDTO>>(quiniela));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtener la guia
        /// </summary>
        [Authorize]
        [ActionName("guide")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetQuinielaGuide([NakedBody]string jsonModel/*int*/)
        {
            HttpResponseMessage response;

            try
            {

                int drawNumber = 0;

                if (!int.TryParse(jsonModel.Decrypt(), out drawNumber))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var quiniela = await _manager.GetQuinielaGuide(drawNumber);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var lst = new List<MinuteResultDTO>();

                var item = new QuinielaResultDTO
                {
                    DrawNumber = 1, 
                    Goals = 2, 
                    MatchResults = null
                };
                //lst.Add(item);

                var quiniela = item;

#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<IEnumerable<MatchGuideDTO>>(quiniela));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

    }
}

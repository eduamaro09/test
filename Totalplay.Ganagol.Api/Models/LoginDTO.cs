﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Televisa.Trebol.Api.Models
{
    public class LoginDTO
    {
        public String Username { get; set; }
        public String Password { get; set; }
    }
}
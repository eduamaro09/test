﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Televisa.Trebol.Api.Models
{
    public class ForResourceDTO
    {
        public string ResourceSet { get; set; }
        public string ResourceKey { get; set; }
        public int Locale { get; set; }
    }
}
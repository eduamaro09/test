﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Televisa.Trebol.Api.Models;

namespace Televisa.Trebol.Api.Support.CustomValidators
{
    public class SearchByStateCountryValidator : AbstractValidator<SearchByStateCountryDTO>
    {
        public SearchByStateCountryValidator()
        {
            RuleFor(l => l.State).NotEmpty();
            RuleFor(l => l.Country).NotEmpty();
        }
    }
}
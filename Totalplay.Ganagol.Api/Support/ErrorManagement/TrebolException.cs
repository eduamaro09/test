﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Televisa.Trebol.Api.Support.ErrorManagement
{
    public class TrebolException : Exception
    {
        public int Code { get; set; }
        public IEnumerable Errors { get; set; }
        public HttpStatusCode HttpCode { get; set; }
        public TrebolException(String message, int code, HttpStatusCode httpcode = HttpStatusCode.BadRequest) : base(message)
        {
            this.Code = code;
            this.HttpCode = httpcode;
            this.Errors = new List<String>() { message };
        }

        public TrebolException(String message, int code, IEnumerable errors) : base(message)
        {
            this.Code = code;
            this.Errors = errors;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;

using Televisa.Trebol.Bussiness;
using Televisa.Trebol.Business.Entities;
using Televisa.Trebol.Bussiness.Validators;
using Televisa.Trebol.Contracts;
using Televisa.Trebol.Contracts.DataAccess;

using Televisa.Trebol.Api.Models;
using Televisa.Trebol.Api.Support.ErrorManagement;

using Microsoft.AspNet.Identity;
using System.Web.Http.ModelBinding;
using AutoMapper;
using System.Web.Http.Description;
using Televisa.Trebol.Api.Support.CustomValidators;
using Televisa.Trebol.Api.Support.Providers;

namespace Televisa.Trebol.Api.Controllers
{
    /// <summary>
    /// Administracion de sitios de tiendas.
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class DealerStoreController : ApiController
    {
        private IStoreDao _storeDao;
        private IStoreManager _storeManager;
        IMapper _mapper;

        private IAuthenticationManager Autentication => Request.GetOwinContext().Authentication;

        public DealerStoreController(IStoreManager storeManager, IStoreDao storeDao, IMapper mapper)
        {
            _storeDao = storeDao;
            _storeManager = storeManager;
            _mapper = mapper;
        }

        /// <summary>
        /// Buscar sítio por estado y municipio
        /// </summary>
        [Authorize]
        [ActionName("searchbystatecountry")]
        [HttpPost]
        public async Task<HttpResponseMessage> SearchByStateCoutry([NakedBody]string jsonModel/*SearchByStateCountryDTO*/)
        {
            HttpResponseMessage response;

            try
            {

                SearchByStateCountryDTO model = jsonModel.ToString().Decrypt().JsonParse<SearchByStateCountryDTO>();

#if DEBUG
                SearchByStateCountryValidator validator = new Support.CustomValidators.SearchByStateCountryValidator();
                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }



                var stores = await _storeManager.SearchByCounty(model.State, model.Country);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

                List<StoreDTO> stores = new List<StoreDTO>();
                var dealerStore = new StoreDTO
                {
                    City = "Ciudad", 
                    Countyname = "Municipio", 
                    //OwnerName = "Noombre Propietario", 
                    RetailerId = 1, 
                    RetailerStatus = "Estatus", 
                    //Row_ID = 1, 
                    StateName = "Estado", 
                    Street1 = "Calle 1", 
                    Street2 = "Calle 2",
                    Zip = "Codigo Postal",
                    
                };
                stores.Add(dealerStore);

#endif

                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<IEnumerable<StoreDTO>>(stores));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Buscar sítio por código postal
        /// </summary>
        [Authorize]
        [ActionName("searchbyzipcode")]
        [HttpPost]
        public async Task<HttpResponseMessage> SearchByZipCode([NakedBody]string jsonModel/*string*/)
        {
            HttpResponseMessage response;

            try
            {

#if DEBUG
                string zipCode = jsonModel.Decrypt();

                var stores = await _storeManager.SearchByZipCode(zipCode);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);


                List<StoreDTO> stores = new List<StoreDTO>();
                var dealerStore = new StoreDTO
                {
                    City = "Ciudad",
                    Countyname = "Municipio",
                    //OwnerName = "Noombre Propietario",
                    RetailerId = 1,
                    RetailerStatus = "Estatus",
                    //Row_ID = 1,
                    StateName = "Estado",
                    Street1 = "Calle 1",
                    Street2 = "Calle 2",
                    Zip = "Codigo Postal",
                };
                stores.Add(dealerStore);

               

#endif

                response = Request.CreateResponse(HttpStatusCode.OK, stores);
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }
    }
}

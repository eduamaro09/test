﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;

using Televisa.Trebol.Bussiness;
using Televisa.Trebol.Business.Entities;
using Televisa.Trebol.Bussiness.Validators;
using Televisa.Trebol.Contracts;
using Televisa.Trebol.Contracts.DataAccess;

using Televisa.Trebol.Api.Models;
using Televisa.Trebol.Api.Support.ErrorManagement;

using Microsoft.AspNet.Identity;
using System.Web.Http.ModelBinding;
using AutoMapper;
using System.Web.Http.Description;
using Televisa.Trebol.Api.Support.Providers;

namespace Televisa.Trebol.Api.Controllers
{
    /// <summary>
    /// Para agregar crédito del Jugador  
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class CreditController : ApiController
    {
        private ICreditDao _creditDao;
        private IPlayerDao _playerDao;
        private IResourceDao _resourceDao;
        private ICreditManager _creditManager;
        private IAuthenticationManager Autentication => Request.GetOwinContext().Authentication;
        IMapper _mapper;

        public CreditController(ICreditManager creditManager, ICreditDao credit, IPlayerDao player, IResourceDao resource, IMapper mapper)
        {
            _creditDao = credit;
            _playerDao = player;
            _resourceDao = resource;
            _creditManager = creditManager;
            _mapper = mapper;
        }

        /// <summary>
        /// Realiza el proceso de adquisición de crédito
        /// </summary>
        [Authorize]
        [ActionName("processcredit")]
        [HttpPost]
        public async Task<HttpResponseMessage> ProcessCreditRequest([NakedBody]string jsonModel/*CreditRequest*/)
        {
            HttpResponseMessage response;

            try
            {

                CreditRequest model = jsonModel.ToString().Decrypt().JsonParse<CreditRequest>();


                CreditRequestValidator validator = new CreditRequestValidator();
                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }

#if DEBUG
                await _creditManager.ProcessCreditRequest(model);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

#endif
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<bool>(true, "Se realizó la adquisición de crédito de forma exitosa"));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;


        }

    }
}



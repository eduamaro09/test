﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Televisa.Trebol.Api.Models;

namespace Televisa.Trebol.Api.Support.CustomValidators
{
    public class PlayerByExternalLoginValidator : AbstractValidator<PlayerByExternalLoginDTO>
    {
        public PlayerByExternalLoginValidator()
        {
            RuleFor(l => l.ExternalUserId).NotEmpty();
            //RuleFor(l => l.LoginProvider).GreaterThan(0);
        }
    }
}
﻿[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Televisa.Trebol.Api.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Televisa.Trebol.Api.NinjectWebCommon), "Stop")]
namespace Televisa.Trebol.Api
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using AutoMapper;
    using Televisa.Trebol.Contracts.DataAccess;
    using Televisa.Trebol.DataAccess;
    using Televisa.Trebol.Bussiness.Mappers;
    using Televisa.Trebol.Contracts;
    using Televisa.Trebol.Bussiness;
    using System.Web.Http;
    using Support.DependInjection;
    using log4net;
    using Entities;
    using System.Configuration;

    public static class NinjectWebCommon
    {
        public static readonly Bootstrapper bootstrapper = new Bootstrapper();
        //Code is recommendation by the client
        private static readonly log4net.ILog apiLogger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver(kernel);

                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile(new AutoMapperProfile());
            });
            var mapper = config.CreateMapper();
            kernel.Bind<IMapper>().ToConstant(mapper);

            kernel.Bind<IPlayerDao>().To<PlayerDao>();
            kernel.Bind<IPlayerManager>().To<PlayerManager>();

            kernel.Bind<IPlayerHistoryDao>().To<PlayerHistoryDao>();
            kernel.Bind<IPlayerHistoryManager>().To<PlayerHistoryManager>();

            kernel.Bind<IDrawDao>().To<DrawDao>();
            kernel.Bind<IDrawManager>().To<DrawManager>();

            kernel.Bind<IStoreDao>().To<StoreDao>();
            kernel.Bind<IStoreManager>().To<StoreManager>();

            kernel.Bind<ICreditDao>().To<CreditDao>();
            kernel.Bind<IResourceDao>().To<ResourceDao>();
            kernel.Bind<ICreditManager>().To <CreditManager>();

            kernel.Bind<ICartManager>().To<CartManager>();
            kernel.Bind<ICartDao>().To<CartDao>();

            kernel.Bind<ICashOutManager>().To<CashOutManager>();
            kernel.Bind<ICashOutDao>().To<CashOutDao>();

            kernel.Bind<IQuinielaManager>().To<QuinielaManager>();
            kernel.Bind<IQuinielaDao>().To<QuinielaDao>();

            kernel.Bind<IResultManager>().To<ResultManager>();
            kernel.Bind<IResultDao>().To<ResultDao>();

            kernel.Bind<ITeamManager>().To<TeamManager>();
            kernel.Bind<ITeamIconDao>().To<TeamIconDao>();


            kernel.Bind<PasswordPolicy>().ToConstant(CreatePasswordPolicy());
            kernel.Bind<ILog>().ToConstant(apiLogger);

        }

        public static PasswordPolicy CreatePasswordPolicy()
        {
            int minDigits = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["PasswordMinDigits"]) ? 1 : int.Parse(ConfigurationManager.AppSettings["PasswordMinDigits"]));
            int minLength = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["PasswordMinLength"]) ? 1 : int.Parse(ConfigurationManager.AppSettings["PasswordMinLength"]));
            int minLowerCase = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["PasswordMinLowerCase"]) ? 1 : int.Parse(ConfigurationManager.AppSettings["PasswordMinLowerCase"]));
            int minSymbol = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["PasswordMinSymbol"]) ? 1 : int.Parse(ConfigurationManager.AppSettings["PasswordMinSymbol"]));
            int minUpperCase = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["PasswordMinUpperCase"]) ? 1 : int.Parse(ConfigurationManager.AppSettings["PasswordMinUpperCase"]));

            return new PasswordPolicy {
                MinDigits = minDigits,
                MinLength = minLength,
                MinLowerCase = minLowerCase,
                MinSymbol = minSymbol,
                MinUpperCase = minUpperCase,
            };
        }
    }
}
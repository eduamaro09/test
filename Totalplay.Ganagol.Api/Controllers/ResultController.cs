﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;

using Televisa.Trebol.Business.Entities;
using Televisa.Trebol.Bussiness.Validators;
using Televisa.Trebol.Bussiness.Validators.Requests;
using Televisa.Trebol.Contracts;
using Televisa.Trebol.Contracts.DataAccess;

using Televisa.Trebol.Api.Models;
using Televisa.Trebol.Api.Support.CustomValidators;
using Televisa.Trebol.Api.Support.ErrorManagement;
using FluentValidation.Results;
using Televisa.Trebol.Entities.DTOs;
using AutoMapper;
using System.Web.Http.Description;
using Televisa.Trebol.Api.Support.Providers;

namespace Televisa.Trebol.Api.Controllers
{
    /// <summary>
    /// obtener el ultimo resultado de los juegos 
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class ResultController : ApiController
    {
        private IResultDao _resultDao;
        private IResultManager _manager;
        IMapper _mapper;

        private IAuthenticationManager Autentication => Request.GetOwinContext().Authentication;

        public ResultController(IResultManager manager, IResultDao resultDao, IMapper mapper)
        {
            _manager = manager;
            _resultDao = resultDao;
            _mapper = mapper;
        }

        /// <summary>
        /// Obtener el ultimo resultado del jugador
        /// </summary>
        [Authorize]
        [HttpPost]
        [ActionName("latest")]
        public async Task<HttpResponseMessage> GetLatest([NakedBody]string jsonModel/*int*/)
        {

            HttpResponseMessage response;

            try
            {
                var username = User.Identity.Name;

                int game = 0;

                if (!int.TryParse(jsonModel.Decrypt(), out game))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }



#if DEBUG
                var result = await _manager.GetLatest((Game)game);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var result = new DrawResultDTO {
                    DrawNumber = 1,
                    DTClosed = DateTime.Now,
                    Game = Game.GanaGol,
                    WinningNumbers = null
                };
#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<DrawResultDTO>(result));

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;

        }


        /// <summary>
        /// Obtener los resultados para el jugador por juego y numero de apuesta
        /// </summary>
        [Authorize]
        [HttpPost]
        [ActionName("drawresults")]
        public async Task<HttpResponseMessage> GetDrawResults([NakedBody]string jsonModel/*DrawResultsDTO*/)
        {

            HttpResponseMessage response;

            try
            {
                var username = User.Identity.Name;

                DrawResultsValidator validator = new DrawResultsValidator();
                DrawResultsDTO model = jsonModel.ToString().Decrypt().JsonParse<DrawResultsDTO>();

                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }



#if DEBUG
                var results = await _manager.GetDrawResults((Game)model.GameId, model.DrawNumber);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var result = new DrawResultDTO {
                    DrawNumber = 1,
                    DTClosed = DateTime.Now,
                    Game = Game.GanaGol,
                    WinningNumbers = null
                };
#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<DrawResultDTO>(results));

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;

        }


    }
}

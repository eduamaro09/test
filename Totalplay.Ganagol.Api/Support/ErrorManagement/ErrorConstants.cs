﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Televisa.Trebol.Api.Support.ErrorManagement
{
    public class ErrorConstants
    {
        public static String ServerError = "Ha ocurrido un error inesperado";
        public static int ServerErrorCode = 1000;

        public static String UserDontExistsError = "El usuario no existe";
        public static int UserDontExistsErrorCode = 1001;

        public static String RegisterError = "Hubo un problema al registrar al usuario";
        public static int RegisterErrorCode = 1002;

        public static String UpdatePlayerError = "Hubo un problema al actualizar al usuario";
        public static int UpdatePlayerErrorCode = 1003;

        public static String UpdatePasswordError = "Hubo un problema al actualizar la contraseña";
        public static int UpdatePasswordErrorCode = 1004;

        public static String UpdateSecretError = "Hubo un problema al actualizar la pregunta secreta";
        public static int UpdateSecretErrorCode = 1004;

        public static String PasswordRecoveryError = "Hubo un problema al recuperar la contraseña";
        public static int PasswordRecoveryErrorCode = 1005;
        public static String SecretQuestionRecovery = "Hubo un problema al actualizar la pregunta secreta";
        public static int SecretQuestionRecoveryErrorCode = 1006;

    }
}
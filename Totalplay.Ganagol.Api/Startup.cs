﻿using Microsoft.Owin;
using Owin;


[assembly: OwinStartupAttribute(typeof(Televisa.Trebol.Api.Startup))]
namespace Televisa.Trebol.Api
{

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
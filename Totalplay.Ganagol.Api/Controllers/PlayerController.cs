﻿// ***********************************************************************
// Assembly         : Televisa.Trebol.Api
// Author           : jfernandez
// Created          : 09-19-2016
//
// Last Modified By : jfernandez
// Last Modified On : 10-06-2016
// ***********************************************************************
// <copyright file="PlayerController.cs" company="GIGIGO">
//     Copyright ©  2016
// </copyright>
// <summary>Class used for a player management</summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;

using Televisa.Trebol.Business.Entities;
using Televisa.Trebol.Bussiness.Validators;
using Televisa.Trebol.Bussiness.Validators.Requests;
using Televisa.Trebol.Contracts;
using Televisa.Trebol.Contracts.DataAccess;

using Televisa.Trebol.Api.Models;
using Televisa.Trebol.Api.Support.CustomValidators;
using Televisa.Trebol.Api.Support.ErrorManagement;
using FluentValidation.Results;
using Televisa.Trebol.Entities.DTOs;
using AutoMapper;
using Televisa.Trebol.Api.Support.Providers;
using Newtonsoft.Json;
using log4net;
using Televisa.Trebol.Entities;

namespace Televisa.Trebol.Api.Controllers
{
    /// <summary>
    /// Administra los datos del Jugador y el acceso a la aplicación
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class PlayerController : ApiController 
    {
        private IPlayerManager _playerManager;
        private IPlayerDao _playerDao;
        private IMapper _mapper;
        private ILog _log;
        private PasswordPolicy _passwordPolicy;
        private IAuthenticationManager Autentication => Request.GetOwinContext().Authentication;


        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerController"/> class.
        /// </summary>
        public PlayerController(IPlayerManager playerManager, IPlayerDao playerDao, ILog log, IMapper mapper, PasswordPolicy passwordPolicy)
        {
            _playerManager = playerManager;
            _playerDao = playerDao;
            _mapper = mapper;
            _log = log;
            _passwordPolicy = passwordPolicy;
        }


        /// <summary>
        /// Login de jugador registrado
        /// </summary>
        [HttpPost]
        [ActionName("login")]
        public async Task<HttpResponseMessage> Login([NakedBody]string jsonModel /*LoginDTO*/)
        {
            HttpResponseMessage response = null;
            try
            {
                LoginDTO model = jsonModel.ToString().Decrypt().JsonParse<LoginDTO>();

                ClaimsIdentity identity = new ClaimsIdentity(Startup.OAuthOptions.AuthenticationType);

                LoginValidator validator = new LoginValidator();
                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }

#if DEBUG 
                var loginResult = await _playerManager.Login(model.Username, model.Password);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var loginResult = LoginResult.Success;
#endif

                if (loginResult == LoginResult.Success || loginResult == LoginResult.SuccessClosedSession)
                {
                    AuthenticationTicket ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
                    var currentUtc = new SystemClock().UtcNow;
                    ticket.Properties.IssuedUtc = currentUtc;
                    ticket.Properties.ExpiresUtc = currentUtc.Add(TimeSpan.FromMinutes(30));

#if DEBUG
                    var player = await _playerManager.GetPlayer(model.Username);


#elif NO_IMPLEMENT_DLL
                    var player = new PlayerDTO()
                    {
                        Firstname = "Miguel Angel",
                        Fullname = "Miguel Angel Cubillos Reynoso",
                        PlayerId = 1,
                        Balance = new PlayerBalanceDTO {
                            Amount = 100.00M,
                            AvailableToWithdraw = 100.00M,
                            CartAmount = 100.00M,
                            Withdrawn = 100.00M,
                            FreeGG = 1,
                            FreeGGP = 1,
                            FreeGM = 1,
                            FreeSL = 1,
                        },
                        Birthday = DateTime.Now.AddYears(-20),
                        Email = "test@gigigo.com.mx", 
                        Gender = "",
                        IsVip = false, 
                        GUID = Guid.NewGuid(), 
                        LoggedIn = false, 
                        Surname = "Cubillos Reynoso", 
                        UserIPAddress = "127.0.0.0", 
                        PasswordQuestion = "¿Pregunta para password?"
                    };
#endif
                    identity.AddClaim(new Claim(ClaimTypes.Name, model.Username));
                    identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, player.PlayerId.ToString()));

                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<object>(new
                    {
                        UserName = model.Username,
                        Login = loginResult,
                        AccessToken = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket),
                        FullName = player.Fullname, 
                        player.PlayerId, 
                        player.Balance
                    }));

                }
                else if (loginResult == LoginResult.Failure)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(model, "Usuario o Contraseña incorrectos.", HttpStatusCode.Unauthorized));
                }
            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }


        /// <summary>
        /// Registro de Jugador
        /// </summary>
        [HttpPost]
        [ActionName("register")]
        public async Task<HttpResponseMessage> Register([NakedBody]string jsonModel/*NewPlayerDTO*/)
        {

            HttpResponseMessage response;

            try
            {
                NewPlayerDTO model = jsonModel.ToString().Decrypt().JsonParse<NewPlayerDTO>();


                NewPlayerValidator validator = new NewPlayerValidator(_passwordPolicy);
                validator.Custom(player => { return (player.IsAdult()) ? null : new ValidationFailure("Edad", "La persona no tiene la mayoría de edad."); });

                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }

#if DEBUG
                var registed = await _playerManager.RegisterPlayer(model);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var registed = 0;
#endif
                if (registed == 0)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<bool>(true, "Tu usuario fue registrado con éxito", HttpStatusCode.OK));
                }
                else
                {
                    throw new TrebolException(ErrorConstants.RegisterError, ErrorConstants.RegisterErrorCode);
                }

            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Registro de Jugador a un servicio externo
        /// </summary>
        [HttpPost]
        [ActionName("registertoexternal")]
        public async Task<HttpResponseMessage> RegisterPlayerWithExternalLogin([NakedBody]string jsonModel/*PlayerExternalLoginDTO*/)
        {

            HttpResponseMessage response;

            try
            {
                //model.IsApproved = false;
                PlayerExternalLoginValidator validator = new PlayerExternalLoginValidator();
                PlayerExternalLoginDTO model = jsonModel.ToString().Decrypt().JsonParse<PlayerExternalLoginDTO>();

                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }

#if DEBUG
                var registed = await _playerManager.RegisterPlayerWithExternalLogin(model);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var registed = 1;
#endif
                if (registed == 0)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<bool>(true, "Tu usuario fue registrado con éxito", HttpStatusCode.OK));
                }
                else
                {
                    throw new TrebolException(ErrorConstants.RegisterError, ErrorConstants.RegisterErrorCode);
                }

            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Asociar un usuario a un servicio externo
        /// </summary>
        [HttpPost]
        [ActionName("linktoexternal")]
        public async Task<HttpResponseMessage> LinkToExternal([NakedBody]string jsonModel/*PlayerExternalLoginDTO*/)
        {

            HttpResponseMessage response;

            try
            {
                //model.IsApproved = false;
                PlayerExternalLoginValidator validator = new PlayerExternalLoginValidator();
                PlayerExternalLoginDTO model = jsonModel.ToString().Decrypt().JsonParse<PlayerExternalLoginDTO>();

                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }

#if DEBUG
                var registed = await _playerManager.LinkPlayerExternalLogin(model);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var registed = 1;
#endif
                if (registed == 0)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<bool>(true, "Tu usuario fue registrado con éxito", HttpStatusCode.OK));
                }
                else
                {
                    throw new TrebolException(ErrorConstants.RegisterError, ErrorConstants.RegisterErrorCode);
                }

            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtener el jugador que accedio a la aplicación
        /// </summary>
        [Authorize]
        [HttpPost]
        [ActionName("profile")]
        public async Task<HttpResponseMessage> GetPlayer()
        {
            HttpResponseMessage response = null;

            try
            {
                var username = User.Identity.Name;

#if DEBUG
                var registed = await _playerManager.GetPlayer(username);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var registed = new PlayerDTO()
                {
                    Firstname = "Miguel Angel",
                    Fullname = "Miguel Angel Cubillos Reynoso",
                    PlayerId = 1,
                    Balance = new PlayerBalanceDTO
                    {
                        Amount = 100.00M,
                        AvailableToWithdraw = 100.00M,
                        CartAmount = 100.00M,
                        Withdrawn = 100.00M,
                        FreeGG = 1,
                        FreeGGP = 1,
                        FreeGM = 1,
                        FreeSL = 1,
                    },
                    Birthday = DateTime.Now.AddYears(-20),
                    Email = "test@gigigo.com.mx",
                    Gender = "",
                    IsVip = false,
                    GUID = Guid.NewGuid(),
                    LoggedIn = false,
                    Surname = "Cubillos Reynoso",
                    UserIPAddress = "127.0.0.0",
                    PasswordQuestion = "¿Pregunta para password?"
                };
#endif

                if (registed != null)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<object>(registed));
                }
                else
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(username, ErrorConstants.UserDontExistsError, HttpStatusCode.NotFound));
                }

            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;

        }

        /// <summary>
        /// Obtener el balance jugador que accedio a la aplicación
        /// </summary>
        [Authorize]
        [HttpPost]
        [ActionName("profilebalance")]
        public async Task<HttpResponseMessage> GetPlayerBalance()
        {

            HttpResponseMessage response;

            try
            {
                var username = User.Identity.Name;

#if DEBUG
                var registed = await _playerManager.GetPlayer(username);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var registed = new PlayerDTO()
                {
                    Firstname = "Miguel Angel",
                    Fullname = "Miguel Angel Cubillos Reynoso",
                    PlayerId = 1,
                    Balance = new PlayerBalanceDTO
                    {
                        Amount = 100.00M,
                        AvailableToWithdraw = 100.00M,
                        CartAmount = 100.00M,
                        Withdrawn = 100.00M,
                        FreeGG = 1,
                        FreeGGP = 1,
                        FreeGM = 1,
                        FreeSL = 1,
                    },
                    Birthday = DateTime.Now.AddYears(-20),
                    Email = "test@gigigo.com.mx",
                    Gender = "",
                    IsVip = false,
                    GUID = Guid.NewGuid(),
                    LoggedIn = false,
                    Surname = "Cubillos Reynoso",
                    UserIPAddress = "127.0.0.0"
                };
#endif

                if (registed != null)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<object>(registed.Balance));
                }
                else
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(username, ErrorConstants.UserDontExistsError, HttpStatusCode.NotFound));
                }
            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }


        /// <summary>
        /// Obtener el Id jugador por medio de su servicio externo asocciado
        /// </summary>
        [Authorize]
        [HttpPost]
        [ActionName("externalidbyexternallogin")]
        public async Task<HttpResponseMessage> GetPlayerIdByExternalLogin([NakedBody]string jsonModel)
        {

            HttpResponseMessage response;

            try
            {
                var username = User.Identity.Name;
                PlayerByExternalLoginDTO model = jsonModel.ToString().Decrypt().JsonParse<PlayerByExternalLoginDTO>();

                PlayerByExternalLoginValidator validator = new PlayerByExternalLoginValidator();

                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }


#if DEBUG
                var registed = await _playerManager.GetPlayerIdByExternalLogin((LoginProvider)model.LoginProvider, model.ExternalUserId);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var registed = 1;
#endif

                if (registed > 0)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<int>(registed));
                }
                else
                {
                    throw new TrebolException(ErrorConstants.UserDontExistsError, ErrorConstants.UserDontExistsErrorCode);
                }

            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;

        }

        /// <summary>
        /// Obtener el Id jugador por medio de su username
        /// </summary>
        [Authorize]
        [HttpPost]
        [ActionName("externalidbyusername")]
        public async Task<HttpResponseMessage> GetPlayerIdByUserName([NakedBody]string jsonModel)
        {

            HttpResponseMessage response;

            try
            {
                var username = jsonModel.Decrypt();// User.Identity.Name;

#if DEBUG
                var registed = await _playerManager.GetPlayerIdByUserName(username);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var registed = 1;
#endif

                if (registed > 0)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<int>(registed));
                }
                else
                {
                    throw new TrebolException(ErrorConstants.UserDontExistsError, ErrorConstants.UserDontExistsErrorCode);
                }

            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;

        }

        /// <summary>
        /// Obtener pregunta secreta del jugador por medio del username
        /// </summary>
        [HttpPost]
        [ActionName("playersecretquestion")]
        public async Task<HttpResponseMessage> GetPlayerSecretQuestion([NakedBody]string jsonModel)
        {

            HttpResponseMessage response;

            try
            {
                var usernameDec = jsonModel.Decrypt();

#if DEBUG
                var question = await _playerManager.GetPlayerSecretQuestion(usernameDec);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var question = "¿Pregunta Secreta Demo?";
#endif

                if (!string.IsNullOrEmpty(question))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<string>(question));
                }
                else
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation("{}", "No se encontro la pregunta para el usuario solicitado.", HttpStatusCode.BadRequest));
                }

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;

        }

        /// <summary>
        /// Actualizar los datos del jugador
        /// </summary>
        [Authorize]
        [HttpPost]
        [ActionName("update")]
        public async Task<HttpResponseMessage> UpdatePlayer([NakedBody]string jsonModel)
        {
            HttpResponseMessage response;

            try
            {
                PlayerDataUpdateRequest model = jsonModel.ToString().Decrypt().JsonParse<PlayerDataUpdateRequest>();

                var username = User.Identity.Name;

                PlayerUpdateRequestValidator validator = new PlayerUpdateRequestValidator();

                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }



#if DEBUG
                var player = await _playerManager.GetPlayer(username);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var player = new PlayerDTO { Firstname = "Player Demo" };
#endif


#if DEBUG
                bool success = await _playerManager.UpdatePlayerPersonalData(model);
#elif NO_IMPLEMENT_DLL
                bool success = true;
#endif
                if (success)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<bool>(success, "Se actualizó su usuario con éxito", HttpStatusCode.OK));
                }
                else
                {
                    throw new TrebolException(ErrorConstants.UpdatePlayerError, ErrorConstants.UpdatePlayerErrorCode);
                }
            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;

        }

        /// <summary>
        /// Actualizar la pregunta secreta del jugador
        /// </summary>
        [Authorize]
        [HttpPost]
        [ActionName("secretquestionupdate")]
        public async Task<HttpResponseMessage> UpdateSecretQuestion([NakedBody]string jsonModel)
        {

            HttpResponseMessage response;

            try
            {
                UpdatePlayerSecretQuestionRequest model = jsonModel.ToString().Decrypt().JsonParse<UpdatePlayerSecretQuestionRequest>();


                var username = User.Identity.Name;
                model.UserName = username;

                UpdatePlayerSecretQuestionRequestValidator validator = new UpdatePlayerSecretQuestionRequestValidator(_playerDao);
                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }

#if DEBUG
                bool success = await _playerManager.UpdatePlayerSecretQuestion(model);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var success =  true;
#endif
                if (success)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<bool>(success, "La pregunta secreta fue actualizada con éxito", HttpStatusCode.OK));
                }
                else
                {
                    throw new TrebolException(ErrorConstants.UpdateSecretError, ErrorConstants.UpdateSecretErrorCode);
                }
            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;

        }

        /// <summary>
        /// Actualizar la contraseña del jugador
        /// </summary>
        [Authorize]
        [HttpPost]
        [ActionName("passwordupdate")]
        public async Task<HttpResponseMessage> UpdatePassword([NakedBody]string jsonModel)
        {

            HttpResponseMessage response;

            try
            {
                UpdatePlayerPasswordRequest model = jsonModel.ToString().Decrypt().JsonParse<UpdatePlayerPasswordRequest>();

                var username = User.Identity.Name;
                model.UserName = username;

                UpdatePlayerPasswordRequestValidator validator = new UpdatePlayerPasswordRequestValidator(_playerDao, _passwordPolicy);
                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }

#if DEBUG
                bool success = await _playerManager.UpdatePlayerPassword(model);
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var success = true;
#endif

                if (success)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<bool>(true, "El password fue actualizado con éxito", HttpStatusCode.OK));
                }
                else
                {
                    throw new TrebolException(ErrorConstants.UpdatePasswordError, ErrorConstants.UpdatePasswordErrorCode);
                }
            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;

        }

        /// <summary>
        /// Actualizar las notificaciones del jugador
        /// </summary>
        [Authorize]
        [HttpPut]
        [ActionName("notifications")]
        public async Task<HttpResponseMessage> UpdateNotifications([NakedBody]string jsonModel)
        {

            HttpResponseMessage response;

            try
            {

                IEnumerable<PlayerNotificationDTO> model = jsonModel.ToString().Decrypt().JsonParse<IEnumerable<PlayerNotificationDTO>>();


                var username = User.Identity.Name;
                var player = _playerManager.GetPlayerIdByUserName(username);

#if DEBUG
                bool success = await _playerManager.UpdatePlayerNotifications(player.Id, model);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var success = true;
#endif

                if (success)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<bool>(success, "Se actualizaron las notificaciones con éxito", HttpStatusCode.OK));
                }
                else
                {
                    throw new TrebolException(ErrorConstants.UpdatePasswordError, ErrorConstants.UpdatePasswordErrorCode);
                }
            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;

        }

        /// <summary>
        /// Recuperar el password del jugador
        /// </summary>
        [HttpPost]
        [ActionName("passrecover")]
        public async Task<HttpResponseMessage> PasswordRecovery([NakedBody]string jsonModel)
        {
            HttpResponseMessage response;

            try
            {
                RecoverAccessRequest model = jsonModel.ToString().Decrypt().JsonParse<RecoverAccessRequest>();

                var username = User.Identity.Name;
                model.UserName = username;

                RecoverAccessRequestValidator validator = new RecoverAccessRequestValidator(_passwordPolicy);
                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }
#if DEBUG
                bool success = await _playerManager.RecoverAccess(model);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var success = true;
#endif

                if (success)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<bool>(success, "El password se actualizó correctamente", HttpStatusCode.OK));
                }
                else
                {
                    throw new TrebolException(ErrorConstants.PasswordRecoveryError, ErrorConstants.PasswordRecoveryErrorCode);
                }
            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;

        }


        
          
            [HttpPost]
            [ActionName("encrypt")]
            public async Task<HttpResponseMessage> Encrypt(string value, string key)
            {

                HttpResponseMessage response;

                try
                {
                    await Task.Delay(100);

                    Encryptor encrypt = new Encryptor();

                    string encrypted = encrypt.Encrypt(value, key, Schema.V3);

                    response = Request.CreateResponse(HttpStatusCode.OK, encrypted);

                }
                catch (TrebolException e)
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest,
                    new Error
                    {
                        status = (int)HttpStatusCode.BadRequest,
                        code = e.Code,
                        message = e.Message,
                        developerMessage = e.Message,
                        validationErrors = e.Errors
                    });
                }
                catch (Exception e)
                {
                    response = Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new Error
                    {
                        status = (int)HttpStatusCode.InternalServerError,
                        code = ErrorConstants.ServerErrorCode,
                        message = ErrorConstants.ServerError,
                        developerMessage = e.ToString()
                    });
                }

                return response;

            }

            [HttpPost]
            [ActionName("encrypttoios")]
            public async Task<HttpResponseMessage> EncryptToIOS(string value, string key)
            {

                HttpResponseMessage response;

                try
                {
                    await Task.Delay(100);

                    Encryptor encrypt = new Encryptor();

                    string encrypted = encrypt.EncryptToIOS(value, key, Schema.V3);

                    response = Request.CreateResponse(HttpStatusCode.OK, encrypted);

                }
                catch (TrebolException e)
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest,
                    new Error
                    {
                        status = (int)HttpStatusCode.BadRequest,
                        code = e.Code,
                        message = e.Message,
                        developerMessage = e.Message,
                        validationErrors = e.Errors
                    });
                }
                catch (Exception e)
                {
                    response = Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new Error
                    {
                        status = (int)HttpStatusCode.InternalServerError,
                        code = ErrorConstants.ServerErrorCode,
                        message = ErrorConstants.ServerError,
                        developerMessage = e.ToString()
                    });
                }

                return response;

            }

           
            [HttpPost]
            [ActionName("decrypt")]
            public async Task<HttpResponseMessage> Decrypt(string value, string key)
            {

                HttpResponseMessage response;

                try
                {
                    await Task.Delay(100);

                    value = value.Replace(' ', '+');
                    Decryptor decryptor = new Decryptor();
                    string decrypted = decryptor.Decrypt(value, key);

                    response = Request.CreateResponse(HttpStatusCode.OK, decrypted);

                }
                catch (TrebolException e)
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest,
                    new Error
                    {
                        status = (int)HttpStatusCode.BadRequest,
                        code = e.Code,
                        message = e.Message,
                        developerMessage = e.Message,
                        validationErrors = e.Errors
                    });
                }
                catch (Exception e)
                {
                    response = Request.CreateResponse(HttpStatusCode.InternalServerError,
                    new Error
                    {
                        status = (int)HttpStatusCode.InternalServerError,
                        code = ErrorConstants.ServerErrorCode,
                        message = ErrorConstants.ServerError,
                        developerMessage = e.ToString()
                    });
                }

                return response;

            }

            

    }
}

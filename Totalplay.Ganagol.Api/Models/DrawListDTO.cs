﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Televisa.Trebol.Api.Models
{
    public class DrawListDTO
    {
        public int Take { get; set; }
        public int GameId { get; set; }
    }
}
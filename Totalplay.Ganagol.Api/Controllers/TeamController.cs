﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;

using Televisa.Trebol.Business.Entities;
using Televisa.Trebol.Bussiness.Validators;
using Televisa.Trebol.Bussiness.Validators.Requests;
using Televisa.Trebol.Contracts;
using Televisa.Trebol.Contracts.DataAccess;

using Televisa.Trebol.Api.Models;
using Televisa.Trebol.Api.Support.CustomValidators;
using Televisa.Trebol.Api.Support.ErrorManagement;
using FluentValidation.Results;
using Televisa.Trebol.Entities.DTOs;
using AutoMapper;
using System.Web.Http.Description;
using Televisa.Trebol.Api.Support.Providers;

namespace Televisa.Trebol.Api.Controllers
{
    /// <summary>
    /// Obtener las imagenes de los logos de los equipos de futbol de México 
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class TeamController : ApiController
    {
        private ITeamIconDao _teamIcon;
        private ITeamManager _teamManager;
        private IAuthenticationManager Autentication => Request.GetOwinContext().Authentication;
        IMapper _mapper;

        public TeamController(ITeamManager teamManager, ITeamIconDao teamIcon, IMapper mapper)
        {
            _teamIcon = teamIcon;
            _teamManager = teamManager;
            _mapper = mapper;

        }

        /// <summary>
        /// Obtiene todos los iconos de los equipos de futbol
        /// </summary>
        [HttpPost]
        [ActionName("teamicons")]
        [Authorize]
        public async Task<HttpResponseMessage> GetAllTeamIcons()
        {
            HttpResponseMessage response = null;
            try
            {

#if DEBUG 
                var icons = await _teamManager.GetAllTeamIcons();
#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var lst = new List<TeamIconDTO>();
                lst.Add(
                    new TeamIconDTO {
                        DT = DateTime.Now,
                        File = new byte[0],
                        OriginalFilePath = "Path", 
                        TeamId = 1,
                        Version = 1,
                    }
                );

                var icons = lst;
#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<IEnumerable<TeamIconDTO>>(icons));

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el icono del equipo de futbol
        /// </summary>
        [HttpPost]
        [ActionName("teamicon")]
        [Authorize]
        public async Task<HttpResponseMessage> GetIcon([NakedBody]string jsonModel/*int*/)
        {
            HttpResponseMessage response = null;
            try
            {
                int teamId = 0;

                if (!int.TryParse(jsonModel.Decrypt(), out teamId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }
#if DEBUG 
                var icon = await _teamManager.GetIcon(teamId);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var lst = new List<TeamIconDTO>();
                lst.Add(
                    new TeamIconDTO {
                        DT = DateTime.Now,
                        File = new byte[0],
                        OriginalFilePath = "Path", 
                        TeamId = 1,
                        Version = 1,
                    }
                );

                var icon = lst.FirstOrDefault();
#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<TeamIconDTO>(icon));

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }


    }
}

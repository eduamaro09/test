﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;

using Televisa.Trebol.Business.Entities;
using Televisa.Trebol.Bussiness.Validators;
using Televisa.Trebol.Bussiness.Validators.Requests;
using Televisa.Trebol.Contracts;
using Televisa.Trebol.Contracts.DataAccess;

using Televisa.Trebol.Api.Models;
using Televisa.Trebol.Api.Support.CustomValidators;
using Televisa.Trebol.Api.Support.ErrorManagement;
using FluentValidation.Results;
using Televisa.Trebol.Entities.DTOs;
using AutoMapper;
using System.Web.Http.Description;
using Televisa.Trebol.Api.Support.Providers;

namespace Televisa.Trebol.Api.Controllers
{

    /// <summary>
    /// Cashout :Cuando el usuario tiene premios por mas de 500, premios menores 10000 se cobran en automatico por arriba de eso es por banco.
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class CashOutController : ApiController
    {
        private ICashOutDao _cashOutDao;
        private IPlayerDao _playerDao;
        private IResourceDao _resourceDao;
        private ICashOutManager _cashOutManager;
        IMapper _mapper;

        private IAuthenticationManager Autentication => Request.GetOwinContext().Authentication;

        public CashOutController(ICashOutManager manager, ICashOutDao cashOutDao, IPlayerDao playerDao, IResourceDao resourceDao, IMapper mapper)
        {
            _cashOutDao = cashOutDao;
            _playerDao = playerDao;
            _resourceDao = resourceDao;
            _cashOutManager = manager;
            _mapper = mapper;
        }

        /// <summary>
        /// Realiza el cobro de un premio
        /// </summary>
        [HttpPost]
        [ActionName("processcashout")]
        [Authorize]
        public async Task<HttpResponseMessage> ProcessCashOut([NakedBody]string jsonModel/*CashOutRequestDTO*/)
        {
            HttpResponseMessage response;

            try
            {
                CashOutRequestDTO model = jsonModel.ToString().Decrypt().JsonParse<CashOutRequestDTO>();


                CashOutRequestValidator validator = new CashOutRequestValidator();

                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }

#if DEBUG
                var resp = await _cashOutManager.ProcessCashOut(model);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var resp = new CashOutRequestDTO {
                    Address = null, 
                    Amount = 1.0M,
                    BankAccountNumber = "1234567890",
                    CashOutType = CashOutType.Bank,
                    Ip = "127.0.0.1",
                    PlayerId = 1, 
                    RFC = "XXXX999999X00"
                };
#endif
                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<CashOutResultDTO>(resp));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;

using Televisa.Trebol.Bussiness;
using Televisa.Trebol.Business.Entities;
using Televisa.Trebol.Bussiness.Validators;
using Televisa.Trebol.Contracts;
using Televisa.Trebol.Contracts.DataAccess;

using Televisa.Trebol.Api.Models;
using Televisa.Trebol.Api.Support.ErrorManagement;

using Microsoft.AspNet.Identity;
using System.Web.Http.ModelBinding;
using AutoMapper;
using System.Web.Http.Description;
using Televisa.Trebol.Api.Support.Providers;
using Televisa.Trebol.Api.Support.CustomValidators;
using Televisa.Trebol.Entities.DTOs;

namespace Televisa.Trebol.Api.Controllers
{

    /// <summary>
    /// Para armar la apuesta (por eso pide el sorteo) 
    /// --> Game son los juegos(prodctos) 
    /// --> Excepto Ganagol puedes meter varias apuestas.
    /// --> No hay filtros de transaccion por fecha solo es por Paginación (No se podra buscar por rango de fecha)
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class DrawController : ApiController
    {
        private IDrawDao _drawDao;
        private IDrawManager _drawManager;
        //private IGameTypeDao _gameTypeDao;
        IMapper _mapper;

        private IAuthenticationManager Autentication => Request.GetOwinContext().Authentication;

        public DrawController(IDrawManager drawManager, IDrawDao drawDao, IMapper mapper)
        {
            _drawDao = drawDao;
            _drawManager = drawManager;
            //_gameTypeDao = gameTypeDao;
            _mapper = mapper;
        }

        /// <summary>
        /// Obtiene la puesta actual
        /// </summary>
        [Authorize]
        [ActionName("current")]
        [HttpPost]
        public async Task<HttpResponseMessage> CurrentDraw([NakedBody]string jsonModel/*int*/)
        {
            HttpResponseMessage response;

            try
            {
                int gameTypeId = 0;

                if (!int.TryParse(jsonModel.Decrypt(), out gameTypeId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation("{}", "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG


                var draw = await _drawManager.GetCurrent((Game)gameTypeId);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

                var draw = new DrawDTO {
                    //DrawId = 1,
                    //DTCloseEnd = DateTime.Now,
                    DTCloseStart = DateTime.Now,
                    DTComplete = DateTime.Now,
                    DTOpen = DateTime.Now,
                    DrawNumber = 1, 
                    Game = Game.GanaGol,
                    //LotteryDrawNumber = 1234567890,
                    //GameTypeDto = new GameTypeDTO {
                    //    ExternalName = "Demo",
                    //    ExternalNameLong = "Demo Largo",
                    //    LotteryName = "Demo Name", 
                    //    GameTypeId = 1, 
                    //    LotteryGameTypeId = 1, 
                    //}
                    
                };

#endif
                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<DrawDTO>(draw));

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene la específica
        /// </summary>
        [Authorize]
        [ActionName("specific")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetDraw([NakedBody]string jsonModel/*DrawDTO*/)
        {
            HttpResponseMessage response;

            try
            {
                SpecificDrawDTO model = jsonModel.ToString().Decrypt().JsonParse<SpecificDrawDTO>();

                SpecificDrawValidator validator = new Support.CustomValidators.SpecificDrawValidator();
                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }

#if DEBUG



                var draw = await _drawManager.GetDraw(model.DrawNumber, (Game)model.GameId);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

                var draw = new DrawDTO
                {
                    //DrawId = 1,
                    //DTCloseEnd = DateTime.Now.AddDays(-1),
                    DTCloseStart = DateTime.Now.AddDays(-1),
                    DTComplete = DateTime.Now.AddDays(-1),
                    DTOpen = DateTime.Now.AddDays(-1),
                    DrawNumber = 1, 
                    Game = Game.GanaGol,
                    //LotteryDrawNumber = 1234567890,
                    //GameTypeDto = new GameTypeDTO
                    //{
                    //    ExternalName = "Demo Specific",
                    //    ExternalNameLong = "Demo Largo  Specific",
                    //    LotteryName = "Demo Name Specific",
                    //    GameTypeId = 2,
                    //    LotteryGameTypeId = 2,
                    //}
                };

#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<DrawDTO>(draw));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene lista de apuestas por medio del juego y paginación
        /// </summary>
        [Authorize]
        [ActionName("list")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetDrawList([NakedBody]string jsonModel/*DrawListDTO*/)
        {
            HttpResponseMessage response;

            try
            {

                DrawListDTO model = jsonModel.ToString().Decrypt().JsonParse<DrawListDTO>();

                DrawListValidator validator = new Support.CustomValidators.DrawListValidator();
                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }

#if DEBUG

                var draw = await _drawManager.GetDrawList((Game)model.GameId, model.Take);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);

                var draw = new DrawDTO
                {
                    //DrawId = 1,
                    //DTCloseEnd = DateTime.Now.AddDays(-1),
                    DTCloseStart = DateTime.Now.AddDays(-1),
                    DTComplete = DateTime.Now.AddDays(-1),
                    DTOpen = DateTime.Now.AddDays(-1),
                    DrawNumber = 1, 
                    Game = Game.GanaGol,
                    //LotteryDrawNumber = 1234567890,
                    //GameTypeDto = new GameTypeDTO
                    //{
                    //    ExternalName = "Demo Specific",
                    //    ExternalNameLong = "Demo Largo  Specific",
                    //    LotteryName = "Demo Name Specific",
                    //    GameTypeId = 2,
                    //    LotteryGameTypeId = 2,
                    //}
                };

#endif

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<IEnumerable<DrawListItemDTO>>(draw));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

    }
}

// Uncomment the following to provide samples for PageResult<T>. Must also add the Microsoft.AspNet.WebApi.OData
// package to your project.
////#define Handle_PageResultOfT

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Televisa.Trebol.Api.Models;
using Televisa.Trebol.Business.Entities;
using Televisa.Trebol.Entities.DTOs;
#if Handle_PageResultOfT
using System.Web.Http.OData;
#endif

namespace Televisa.Trebol.Api.Areas.HelpPage
{
    /// <summary>
    /// Use this class to customize the Help Page.
    /// For example you can set a custom <see cref="System.Web.Http.Description.IDocumentationProvider"/> to supply the documentation
    /// or you can provide the samples for the requests/responses.
    /// </summary>
    public static class HelpPageConfig
    {
        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters",
            MessageId = "Televisa.Trebol.Api.Areas.HelpPage.TextSample.#ctor(System.String)",
            Justification = "End users may choose to merge this string with existing localized resources.")]
        [SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly",
            MessageId = "bsonspec",
            Justification = "Part of a URI.")]
        public static void Register(HttpConfiguration config)
        {
            //// Uncomment the following to use the documentation from XML documentation file.
            config.SetDocumentationProvider(new XmlDocumentationProvider(HttpContext.Current.Server.MapPath("~/App_Data/XmlDocument.xml")));

            //// Uncomment the following to use "sample string" as the sample for all actions that have string as the body parameter or return type.
            //// Also, the string arrays will be used for IEnumerable<string>. The sample objects will be serialized into different media type 
            //// formats by the available formatters.
            //config.SetSampleObjects(new Dictionary<Type, object>
            //{
            //    {typeof(string), "sample string"},
            //    {typeof(IEnumerable<string>), new string[]{"sample 1", "sample 2"}}
            //});

            // Extend the following to provide factories for types not handled automatically (those lacking parameterless
            // constructors) or for which you prefer to use non-default property values. Line below provides a fallback
            // since automatic handling will fail and GeneratePageResult handles only a single type.
#if Handle_PageResultOfT
            config.GetHelpPageSampleGenerator().SampleObjectFactories.Add(GeneratePageResult);
#endif

            // Extend the following to use a preset object directly as the sample for all actions that support a media
            // type, regardless of the body parameter or return type. The lines below avoid display of binary content.
            // The BsonMediaTypeFormatter (if available) is not used to serialize the TextSample object.
            config.SetSampleForMediaType(
                new TextSample("Binary JSON content. See http://bsonspec.org for details."),
                new MediaTypeHeaderValue("application/bson"));

            //// Uncomment the following to use "[0]=foo&[1]=bar" directly as the sample for all actions that support form URL encoded format
            //// and have IEnumerable<string> as the body parameter or return type.
            //config.SetSampleForType("[0]=foo&[1]=bar", new MediaTypeHeaderValue("application/json"), typeof(IEnumerable<string>));

            //// Uncomment the following to use "1234" directly as the request sample for media type "text/plain" on the controller named "Values"
            //// and action named "Put".
            //config.SetSampleRequest(Newtonsoft.Json.JsonConvert.SerializeObject(new Api.Models.LoginDTO(), Newtonsoft.Json.Formatting.Indented), new MediaTypeHeaderValue("text/json"), "Player", "login");

            //// Uncomment the following to use the image on "../images/aspNetHome.png" directly as the response sample for media type "image/png"
            //// on the controller named "Values" and action named "Get" with parameter "id".
            //config.SetSampleResponse(new ImageSample("../images/aspNetHome.png"), new MediaTypeHeaderValue("image/png"), "Values", "Get", "id");


            //// PLAYER DEFINED INPUT PARAMETERS
            config.SetActualRequestType(typeof(LoginDTO), "Player", "login");
            config.SetActualRequestType(typeof(NewPlayerDTO), "Player", "register");
            config.SetActualRequestType(typeof(PlayerExternalLoginDTO), "Player", "registertoexternal");
            config.SetActualRequestType(typeof(PlayerExternalLoginDTO), "Player", "linktoexternal");
            config.SetActualRequestType(typeof(PlayerByExternalLoginDTO), "Player", "externalidbyexternallogin");
            config.SetActualRequestType(typeof(string), "Player", "externalidbyusername");
            config.SetActualRequestType(typeof(string), "Player", "playersecretquestion");
            config.SetActualRequestType(typeof(PlayerDataUpdateRequest), "Player", "update");
            config.SetActualRequestType(typeof(UpdatePlayerSecretQuestionRequest), "Player", "secretquestionupdate");
            config.SetActualRequestType(typeof(UpdatePlayerPasswordRequest), "Player", "passwordupdate");
            config.SetActualRequestType(typeof(IEnumerable<PlayerNotificationDTO>), "Player", "notifications");
            config.SetActualRequestType(typeof(RecoverAccessRequest), "Player", "passrecover");

            
            //// PLAYER DEFINED EXPORT PARAMETERS
            config.SetActualResponseType(typeof(PlayerDTO), "Player", "login");
            config.SetActualResponseType(typeof(bool), "Player", "register");
            config.SetActualResponseType(typeof(bool), "Player", "registertoexternal");
            config.SetActualResponseType(typeof(bool), "Player", "linktoexternal");
            config.SetActualResponseType(typeof(PlayerDTO), "Player", "profile");
            config.SetActualResponseType(typeof(PlayerBalanceDTO), "Player", "profilebalance");
            config.SetActualResponseType(typeof(int), "Player", "externalidbyexternallogin");
            config.SetActualResponseType(typeof(int), "Player", "externalidbyusername");
            config.SetActualResponseType(typeof(string), "Player", "playersecretquestion");
            config.SetActualResponseType(typeof(bool), "Player", "update");
            config.SetActualResponseType(typeof(bool), "Player", "secretquestionupdate");
            config.SetActualResponseType(typeof(bool), "Player", "passwordupdate");
            config.SetActualResponseType(typeof(bool), "Player", "notifications");
            config.SetActualResponseType(typeof(bool), "Player", "passrecover");


            //// CARTMANAGER DEFINED INPUT PARAMETERS
            config.SetActualRequestType(typeof(CartTicketDTO), "Cart", "addticket");
            config.SetActualRequestType(typeof(string), "Cart", "buycard");
            config.SetActualRequestType(typeof(int), "Cart", "ggticketdetails");
            config.SetActualRequestType(typeof(int), "Cart", "playercart");
            config.SetActualRequestType(typeof(int), "Cart", "ticketdetails");
            config.SetActualRequestType(typeof(int), "Cart", "removeticket");


            //// CARTMANAGER DEFINED EXPORT PARAMETERS
            config.SetActualResponseType(typeof(int), "Cart", "addticket");
            config.SetActualResponseType(typeof(bool), "Cart", "buycard");
            config.SetActualResponseType(typeof(GGCartBetDetailsDTO), "Cart", "ggticketdetails");
            config.SetActualResponseType(typeof(PlayerCartTicketInfoDTO), "Cart", "playercart");
            config.SetActualResponseType(typeof(IEnumerable<CartBetDetailsDTO>), "Cart", "ticketdetails");
            config.SetActualResponseType(typeof(bool), "Cart", "removeticket");


            //// CASHOUTMANAGER DEFINED INPUT PARAMETERS
            config.SetActualRequestType(typeof(CashOutRequestDTO), "CashOut", "processcashout");

            //// CASHOUTMANAGER DEFINED EXPORT PARAMETERS
            config.SetActualResponseType(typeof(CashOutResultDTO), "CashOut", "processcashout");

            //// CREDITMANAGR DEFINED INPUT PARAMETERS
            config.SetActualRequestType(typeof(CreditRequest), "Credit", "processcredit");

            //// CREDITMANAGR DEFINED EXPORT PARAMETERS
            config.SetActualResponseType(typeof(bool), "Credit", "processcredit");


            //// STOREMANAGER DEFINED INPUT PARAMETERS
            config.SetActualRequestType(typeof(SearchByStateCountryDTO), "DealerStore", "searchbystatecountry");
            config.SetActualRequestType(typeof(string), "DealerStore", "searchbyzipcode");

            //// STOREMANAGER DEFINED EXPORT PARAMETERS
            config.SetActualResponseType(typeof(IEnumerable<StoreDTO>), "DealerStore", "searchbystatecountry");
            config.SetActualResponseType(typeof(IEnumerable<StoreDTO>), "DealerStore", "searchbyzipcode");


            //// DRAWMANAGER DEFINED INPUT PARAMETERS
            config.SetActualRequestType(typeof(int), "Draw", "current");
            config.SetActualRequestType(typeof(SpecificDrawDTO), "Draw", "specific");
            config.SetActualRequestType(typeof(DrawListDTO), "Draw", "list");

            //// DRAWMANAGER DEFINED EXPORT PARAMETERS
            config.SetActualResponseType(typeof(DrawDTO), "Draw", "current");
            config.SetActualResponseType(typeof(DrawDTO), "Draw", "specific");
            config.SetActualResponseType(typeof(IEnumerable<DrawListItemDTO>), "Draw", "list");

            //// PLAYERHISTORY DEFINED INPUT PARAMETERS
            config.SetActualRequestType(typeof(long), "PlayerHistory", "adjustment");
            config.SetActualRequestType(typeof(long), "PlayerHistory", "betcancelticket");
            config.SetActualRequestType(typeof(long), "PlayerHistory", "rejectticket");
            config.SetActualRequestType(typeof(long), "PlayerHistory", "betticket");
            config.SetActualRequestType(typeof(long), "PlayerHistory", "blackhawkcredited");
            config.SetActualRequestType(typeof(long), "PlayerHistory", "cashout");
            config.SetActualRequestType(typeof(long), "PlayerHistory", "cashooutreversal");
            config.SetActualRequestType(typeof(long), "PlayerHistory", "creditcard");
            config.SetActualRequestType(typeof(long), "PlayerHistory", "creditcardreversal");
            config.SetActualRequestType(typeof(long), "PlayerHistory", "dinero");
            config.SetActualRequestType(typeof(HistoryRequestDTO), "PlayerHistory", "history");
            config.SetActualRequestType(typeof(long), "PlayerHistory", "win");
            config.SetActualRequestType(typeof(long), "PlayerHistory", "winunofficial");
            config.SetActualRequestType(typeof(long), "PlayerHistory", "wiretransfer");
            config.SetActualRequestType(typeof(long), "PlayerHistory", "wiretransferreversal");
            config.SetActualRequestType(typeof(long), "PlayerHistory", "worldcupregister");

            //// PLAYERHISTORY DEFINED EXPORT PARAMETERS
            config.SetActualResponseType(typeof(AdjustmentDetailsDTO), "PlayerHistory", "adjustment");
            config.SetActualResponseType(typeof(BetCancelTicketDTO), "PlayerHistory", "betcancelticket");
            config.SetActualResponseType(typeof(BetErrorTicketDTO), "PlayerHistory", "rejectticket");
            config.SetActualResponseType(typeof(BetTicketDTO), "PlayerHistory", "betticket");
            config.SetActualResponseType(typeof(BlackHawkDetailsDTO), "PlayerHistory", "blackhawkcredited");
            config.SetActualResponseType(typeof(CashOutDetailsDTO), "PlayerHistory", "cashout");
            config.SetActualResponseType(typeof(CashOutReversalDetailsDTO), "PlayerHistory", "cashooutreversal");
            config.SetActualResponseType(typeof(CreditCardDetailsDTO), "PlayerHistory", "creditcard");
            config.SetActualResponseType(typeof(CreditCardDetailsDTO), "PlayerHistory", "creditcardreversal");
            config.SetActualResponseType(typeof(DineroDetailsDTO), "PlayerHistory", "dinero");
            config.SetActualResponseType(typeof(PlayerHistoryPageDTO), "PlayerHistory", "history");
            config.SetActualResponseType(typeof(WinDetailsDTO), "PlayerHistory", "win");
            config.SetActualResponseType(typeof(WinUnofficialDetailDTO), "PlayerHistory", "winunofficial");
            config.SetActualResponseType(typeof(WireTransferDetailDTO), "PlayerHistory", "wiretransfer");
            config.SetActualResponseType(typeof(WireTransferDetailDTO), "PlayerHistory", "wiretransferreversal");
            config.SetActualResponseType(typeof(WorlCupRegisterDetailDTO), "PlayerHistory", "worldcupregister");

            //// QUINIELAMANAGER DEFINED INPUT PARAMETERS
            //config.SetActualRequestType(typeof(int), "Quiniela", "current");
            //config.SetActualRequestType(typeof(SpecificDrawDTO), "Quiniela", "minuteresults");
            config.SetActualRequestType(typeof(int), "Quiniela", "quiniela");
            config.SetActualRequestType(typeof(int), "Quiniela", "quinielaresult");
            config.SetActualRequestType(typeof(int), "Quiniela", "odds");
            config.SetActualRequestType(typeof(int), "Quiniela", "guide");

            //// QUINIELAMANAGER DEFINED EXPORT PARAMETERS
            config.SetActualResponseType(typeof(QuinielaDTO), "Quiniela", "current");
            config.SetActualResponseType(typeof(IEnumerable<MinuteResultDTO>), "Quiniela", "minuteresults");
            config.SetActualResponseType(typeof(IEnumerable<GanaGolOddDTO>), "Quiniela", "odds");
            config.SetActualResponseType(typeof(IEnumerable<MatchGuideDTO>), "Quiniela", "guide");


            //// RESULTMANAGER DEFINED INPUT PARAMETERS
            config.SetActualRequestType(typeof(int), "Result", "latest");
            config.SetActualRequestType(typeof(DrawResultsDTO), "Result", "drawresults");

            //// RESULTMANAGER DEFINED EXPORT PARAMETERS
            config.SetActualResponseType(typeof(DrawResultDTO), "Result", "latest");
            config.SetActualResponseType(typeof(DrawResultDTO), "Result", "drawresults");

            //// TEAMMANAGER DEFINED INPUT PARAMETERS
            //config.SetActualRequestType(typeof(int), "Team", "teamicons");
            config.SetActualRequestType(typeof(int), "Team", "teamicon");

            //// TEAMMANAGER DEFINED EXPORT PARAMETERS
            config.SetActualResponseType(typeof(IEnumerable<TeamIconDTO>), "Team", "teamicons");
            config.SetActualResponseType(typeof(TeamIconDTO), "Team", "teamicon");


            //// RESOURCEMANAGER DEFINED INPUT PARAMETERS
            config.SetActualRequestType(typeof(ForResourceDTO), "Resource", "fileresource");
            config.SetActualRequestType(typeof(ForResourceDTO), "Resource", "resource");
            config.SetActualRequestType(typeof(ForResourceDTO), "Resource", "resourcefilename");
            config.SetActualRequestType(typeof(ForResourceDTO), "Resource", "textresource");
            config.SetActualRequestType(typeof(ForResourceDTO), "Resource", "textresourceset");

            //// RESOURCEMANAGER DEFINED EXPORT PARAMETERS
            config.SetActualResponseType(typeof(byte[]), "Resource", "fileresource");
            config.SetActualResponseType(typeof(ResourceDTO), "Resource", "resource");
            config.SetActualResponseType(typeof(string), "Resource", "resourcefilename");
            config.SetActualResponseType(typeof(string), "Resource", "textresource");
            config.SetActualResponseType(typeof(IEnumerable<ResourceDTO>), "Resource", "textresourceset");

        }

#if Handle_PageResultOfT
        private static object GeneratePageResult(HelpPageSampleGenerator sampleGenerator, Type type)
        {
            if (type.IsGenericType)
            {
                Type openGenericType = type.GetGenericTypeDefinition();
                if (openGenericType == typeof(PageResult<>))
                {
                    // Get the T in PageResult<T>
                    Type[] typeParameters = type.GetGenericArguments();
                    Debug.Assert(typeParameters.Length == 1);

                    // Create an enumeration to pass as the first parameter to the PageResult<T> constuctor
                    Type itemsType = typeof(List<>).MakeGenericType(typeParameters);
                    object items = sampleGenerator.GetSampleObject(itemsType);

                    // Fill in the other information needed to invoke the PageResult<T> constuctor
                    Type[] parameterTypes = new Type[] { itemsType, typeof(Uri), typeof(long?), };
                    object[] parameters = new object[] { items, null, (long)ObjectGenerator.DefaultCollectionSize, };

                    // Call PageResult(IEnumerable<T> items, Uri nextPageLink, long? count) constructor
                    ConstructorInfo constructor = type.GetConstructor(parameterTypes);
                    return constructor.Invoke(parameters);
                }
            }

            return null;
        }
#endif
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Televisa.Trebol.Api.Support.Providers
{
    public static class ExtensionMethod
    {
        private static string _encryptKey = "e8ffc7e56311679f";

        public static string Encrypt(this string value)
        {
            try
            {
                Encryptor enc = new Encryptor();

                if (value.Length > 0)
                {
                    return enc.Encrypt(value, _encryptKey, Schema.V3);
                }
                return value;
            }
            catch (Exception e)
            {
                throw new Exception("Error de encripción del dato", e);
            }
           
        }

        public static string Decrypt(this string value)
        {
            try
            {
                Decryptor dec = new Decryptor();

                if (value.Length > 0)
                {
                    return dec.Decrypt(value, _encryptKey);
                }

                return value;
            }
            catch (Exception e)
            {
                throw new Exception("Error de desencripción del dato", e);
            }
        }

        public static T JsonParse<T>(this string val)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(val);
            }
            catch (Exception e)
            {
                throw new Exception("Error de parseo a JSON del dato", e);
            }
        }
    }
}
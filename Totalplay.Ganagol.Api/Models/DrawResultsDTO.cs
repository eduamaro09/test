﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Televisa.Trebol.Api.Models
{
    public class DrawResultsDTO
    {
        public int DrawNumber { get; set; }
        public int GameId { get; set; }
    }
}
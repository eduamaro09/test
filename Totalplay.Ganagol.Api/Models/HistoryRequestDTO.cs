﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Televisa.Trebol.Business.Entities;

namespace Televisa.Trebol.Api.Models
{
    public class HistoryRequestDTO
    {
        public Locale Locale { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
}
﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Totalplay.Ganagol.Api.Controllers;
using Totalplay.Ganagol.Api.Models;
using System.Web.Http;
using Totalplay.Ganagol.Api;
using Totalplay.Ganagol.Api.Support.DependInjection;
using System.Net.Http;
using System.Net.Http.Headers;
using Ninject.Web.Common;
using Microsoft.Owin.Testing;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;

namespace Totalplay.Ganagol.UTests
{
    [TestClass]
    public class TestPlayer : BaseControllerBase
    {
        public TestPlayer()
        {
            SetupHttpServer();
        }

        [TestMethod]
        public async Task Login()
        {
            try
            {
                HttpResponseMessage response;
                var client = new HttpClient(_httpServer);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //var request = CreateRequest(string.Format("api/v1/player/login?Username={0}&Password={1}", "123", "123" ), "application/json", HttpMethod.Post);
                var request = CreateRequestFromObject(string.Format("api/v1/player/login"), new LoginDTO { Password="1234", Username="gigigo@gigigo.com" }, "application/json", HttpMethod.Post);

                using (response = (await client.SendAsync(request, HttpCompletionOption.ResponseContentRead)))
                {
                    Assert.IsNotNull(response.Content);
                    Assert.AreEqual("application/json", response.Content.Headers.ContentType.MediaType);

                    //var result = response.Content.ReadAsAsync<ISandwich>().Result;
                    //Assert.NotNull(result);

                    //Assert.AreEqual(1, result.Id);
                    //Assert.AreEqual("Deli Sandwich", result.Description);
                    //Assert.That(response.IsSuccessStatusCode);
                }

                request.Dispose();
            }
            catch (Exception ex)
            {

                throw ex;
            }
          

        }
    }

    public abstract class BaseControllerBase : IDisposable
    {
        protected HttpServer _httpServer;
        protected const string Url = "http://localhost:19026/";

        protected TestServer _testServer; 

        //[TestFixtureSetUp]
        public void SetupHttpServer()
        {
            var config = new HttpConfiguration();

            config.Routes.MapHttpRoute(name: "Default", routeTemplate: "api/v1/{controller}/{action}/{id}", defaults: new { id = RouteParameter.Optional /*, action = RouteParameter.Optional*/ });


            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
            
            // Here's where we set-up and connect to Ninject as defined in the Web API
            NinjectWebCommon.Start();
            var resolver = new NinjectResolver(NinjectWebCommon.bootstrapper.Kernel);
            config.DependencyResolver = resolver;

            _httpServer = new HttpServer(config);

            _testServer = TestServer.Create<Totalplay.Ganagol.Api.Startup>();
            _testServer.BaseAddress = new Uri(Url);
        }

        public void Dispose()
        {
            if (_httpServer != null)
            {
                _httpServer.Dispose();
            }

            if (_testServer != null)
            {
                _testServer.Dispose();
            }

        }

        /// 

        /// This method is taken from Filip W in a blog post located at: http://www.strathweb.com/2012/06/asp-net-web-api-integration-testing-with-in-memory-hosting/
        /// 

        /// 
        /// 
        /// 
        /// 
        protected HttpRequestMessage CreateRequest(string url, string mthv, HttpMethod method)
        {
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(Url + url)
            };
           
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(mthv));
            request.Method = method;
            return request;
        }

        protected HttpRequestMessage CreateRequestFromObject(string url, Object value, string mthv, HttpMethod method)
        {
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(Url + url)
            };

            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(mthv));
            request.Method = method;
            request.Content = new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json");
            return request;
        }

    }
}

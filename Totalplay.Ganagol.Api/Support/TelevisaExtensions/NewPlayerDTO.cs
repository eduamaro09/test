﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Televisa.Trebol.Business.Entities
{
    public static class EntitiesExtension
    {
        public static bool IsAdult(this NewPlayerDTO value) {
            if (value != null && value.BirthDate != null)
            {
                return ((value.BirthDate.Year - DateTime.Now.Year) <= 18) ? true: false;
            }

            return false;
        }
        
    }

}
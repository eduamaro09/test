﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;

using Televisa.Trebol.Business.Entities;
using Televisa.Trebol.Bussiness.Validators;
using Televisa.Trebol.Bussiness.Validators.Requests;
using Televisa.Trebol.Contracts;
using Televisa.Trebol.Contracts.DataAccess;

using Televisa.Trebol.Api.Models;
using Televisa.Trebol.Api.Support.CustomValidators;
using Televisa.Trebol.Api.Support.ErrorManagement;
using FluentValidation.Results;
using Televisa.Trebol.Entities.DTOs;
using AutoMapper;
using System.Web.Http.Description;
using Televisa.Trebol.Api.Support.Providers;

namespace Televisa.Trebol.Api.Controllers
{
    /// <summary>
    /// Permite el manejo del carrito de compra del jugador
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class CartController : ApiController
    {
        private ICartDao _cartDao;
        private ICartManager _cartManager;
        private IPlayerManager _playerManager;
        private IPlayerDao _playerDao;
        IMapper _mapper;
        private IAuthenticationManager Autentication => Request.GetOwinContext().Authentication;

        public CartController(ICartManager manager, IPlayerManager playerManager, ICartDao cartDao, IPlayerDao playerDao, IMapper mapper)
        {
            _cartManager = manager;
            _cartDao = cartDao;
            _playerManager = playerManager;
            _playerDao = playerDao;
            _mapper = mapper;
        }

        /// <summary>
        /// Agregar un ticket al carrito de compra
        /// </summary>
        [HttpPost]
        [Authorize]
        [ActionName("addticket")]
        public async Task<HttpResponseMessage> AddTicket([NakedBody]string jsonModel/*CartTicketDTO*/)
        {

            HttpResponseMessage response;

            try
            {
                CartTicketDTO model = jsonModel.ToString().Decrypt().JsonParse<CartTicketDTO>();

                //model.IsApproved = false;
                CartTicketValidator validator = new CartTicketValidator();
                var username = User.Identity.Name;
                var player = await _playerManager.GetPlayer(username);


                var result = validator.Validate(model);

                if (!result.IsValid)
                {
                    response = Request.CreateResponse(BaseController.CreateResultForValidation(result.Errors.Select(e => e.ErrorMessage)));
                    return response;
                }

#if DEBUG
                var resp = await _cartManager.AddTicket(player.PlayerId, model);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var resp = 1;
#endif
                if (resp > 0)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<int>(resp, "El ticket se agregó correctamente", HttpStatusCode.OK));
                }
                else
                {
                    throw new TrebolException(ErrorConstants.RegisterError, ErrorConstants.RegisterErrorCode);
                }

            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Realizar la compra del carrito el parametro a enviar es la IP o en su caso el identificador del dispositivo
        /// </summary>
        [HttpPost]
        [Authorize]
        [ActionName("buycard")]
        public async Task<HttpResponseMessage> BuyCart([NakedBody]string jsonModel /*string*/)
        {

            HttpResponseMessage response;

            try
            {
                string ip = jsonModel.Decrypt();


                var username = User.Identity.Name;
                var player = await _playerManager.GetPlayer(username);


                if (string.IsNullOrEmpty(ip))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(false, "Debe indicar el dato de IP", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var result = await _cartManager.BuyCart(player.PlayerId, ip);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var result = true;
#endif
                if (result)
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<bool>(result, "Se realizó su compra de forma exitosa", HttpStatusCode.OK));
                }
                else
                {
                    throw new TrebolException(ErrorConstants.RegisterError, ErrorConstants.RegisterErrorCode);
                }

            }
            catch (TrebolException e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromApplicationException(e));
            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene detalle de un ticket 
        /// </summary>
        [HttpPost]
        [Authorize]
        [ActionName("ggticketdetails")]
        public async Task<HttpResponseMessage> GetGGTicketDetails([NakedBody]string jsonModel/*int*/)
        {

            HttpResponseMessage response;

            try
            {
                int cartTicketId = 0;

                if (!int.TryParse(jsonModel.Decrypt(), out cartTicketId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var result = await _cartManager.GetGGTicketDetails(cartTicketId);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var result = new GGCartBetDetailsDTO {
                    Amount = 1.0M, 
                    BetType = BetType.GanaGol, 
                    DrawNumber = 1, 
                    DT = DateTime.Now, 
                    FreePlays = 1, 
                    Goals = 1, 
                    Selections = null, 
                };
#endif
                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<GGCartBetDetailsDTO>(result));

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }


        /// <summary>
        /// Obtiene el carrito de compra del jugador
        /// </summary>
        [HttpPost]
        [Authorize]
        [ActionName("playercart")]
        public async Task<HttpResponseMessage> GetPlayerCart(/*int*/)
        {

            HttpResponseMessage response;

            try
            {

                var username = User.Identity.Name;
                var player = await _playerManager.GetPlayer(username);


#if DEBUG
                var result = await _cartManager.GetPlayerCart(player.PlayerId);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var result = new PlayerBalanceDTO {
                    Amount = 1.0M, 
                    AvailableToWithdraw = 1.0M, 
                    CartAmount = 1.0M,
                    FreeGG = 1, 
                    FreeGGP = 1, 
                    FreeGM = 1, 
                    FreeSL = 1, 
                    Withdrawn = 1.0M 
                };
#endif
                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<PlayerCartTicketInfoDTO>(result));

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el detalle de un ticket del carrito de compra
        /// </summary>
        [HttpPost]
        [Authorize]
        [ActionName("ticketdetails")]
        public async Task<HttpResponseMessage> GetTicketDetails([NakedBody]string jsonModel/*int*/)
        {

            HttpResponseMessage response;

            try
            {

                int cartTicketId = 0;

                if (!int.TryParse(jsonModel.Decrypt(), out cartTicketId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }

#if DEBUG
                var result = await _cartManager.GetTicketDetails(cartTicketId);

#elif NO_IMPLEMENT_DLL
                await Task.Delay(100);
                var element = new CartBetDetailsDTO {
                    Amount = 1.0M, 
                    BetType = BetType.GanaGol, 
                    DrawNumber = 1, 
                    DT = DateTime.Now, 
                    FreePlays = 1, 
                    Selections = null, 
                    NumberOfDraws = 1, 
                    Panel = 1, 
                    QuickPick = true
                };
                var lst = new List<CartBetDetailsDTO>();
                lst.Add(element);

                var result = lst;

#endif
                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<IEnumerable<CartBetDetailsDTO>>(result));

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }


            return response;
        }

        /// <summary>
        /// Quita un ticket de la lista del carrito de compra
        /// </summary>
        /// <param name="jsonModel">The json model.</param>
        /// <returns>Task&lt;HttpResponseMessage&gt;.</returns>
        [HttpDelete]
        [Authorize]
        [ActionName("removeticket")]
        public async Task<HttpResponseMessage> RemoveTicket([NakedBody]string jsonModel /*int*/)
        {
            HttpResponseMessage response;

            try
            {

                int cartTicketId = 0;

                if (!int.TryParse(jsonModel.Decrypt(), out cartTicketId))
                {
                    response = Request.CreateResponse(BaseController.CreateResultFromBadOperation(jsonModel.Decrypt(), "No se pudo convertir el valor enviado", HttpStatusCode.NotAcceptable));
                    return response;
                }


                var username = User.Identity.Name;
                var player = await _playerManager.GetPlayer(username);

#if DEBUG
                var result = await _cartManager.RemoveTicket(player.PlayerId, cartTicketId);

#elif NO_IMPLEMENT_DLL
               var result = true;

#endif
                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<bool>(true));

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }


            return response;
        }
    }
}

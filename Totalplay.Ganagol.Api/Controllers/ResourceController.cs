﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;

using Televisa.Trebol.Business.Entities;
using Televisa.Trebol.Bussiness.Validators;
using Televisa.Trebol.Bussiness.Validators.Requests;
using Televisa.Trebol.Contracts;
using Televisa.Trebol.Contracts.DataAccess;

using Televisa.Trebol.Api.Models;
using Televisa.Trebol.Api.Support.CustomValidators;
using Televisa.Trebol.Api.Support.ErrorManagement;
using FluentValidation.Results;
using Televisa.Trebol.Entities.DTOs;
using AutoMapper;
using System.Web.Http.Description;
using Televisa.Trebol.Api.Support.Providers;

namespace Televisa.Trebol.Api.Controllers
{
    /// <summary>
    /// Obtener los Resource que son usados por la aplicación
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class ResourceController : ApiController
    {
        private IResourceManager _manager;
        private IResourceDao _resourceDao;
        IMapper _mapper;

        private IAuthenticationManager Autentication => Request.GetOwinContext().Authentication;

        public ResourceController(IResourceManager manager, IResourceDao resourceDao, IMapper mapper)
        {
            _manager = manager;
            _resourceDao = resourceDao;
            _mapper = mapper;
        }

        /// <summary>
        /// Obtiene un recurso en formato de archivo
        /// </summary>
        [HttpPost]
        [ActionName("fileresource")]
        [Authorize]
        public async Task<HttpResponseMessage> GetFileResource([NakedBody]string jsonModel/*int*/)
        {
            HttpResponseMessage response = null;
            try
            {
                ForResourceDTO model = jsonModel.Decrypt().JsonParse<ForResourceDTO>();
                
                    var file = await _manager.GetFileResource(model.ResourceSet, model.ResourceKey, (Locale)model.Locale);

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<byte[]>(file));

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene un recurso 
        /// </summary>
        [HttpPost]
        [ActionName("resource")]
        [Authorize]
        public async Task<HttpResponseMessage> GetResource([NakedBody]string jsonModel/*int*/)
        {
            HttpResponseMessage response = null;
            try
            {
                ForResourceDTO model = jsonModel.Decrypt().JsonParse<ForResourceDTO>();

                var file = await _manager.GetResource(model.ResourceSet, model.ResourceKey, (Locale)model.Locale);

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<ResourceDTO>(file));

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el nombre de un recurso
        /// </summary>
        [HttpPost]
        [ActionName("resourcefilename")]
        [Authorize]
        public async Task<HttpResponseMessage> GetResourceFileName([NakedBody]string jsonModel/*int*/)
        {
            HttpResponseMessage response = null;
            try
            {
                ForResourceDTO model = jsonModel.Decrypt().JsonParse<ForResourceDTO>();

                var file = await _manager.GetResourceFileName(model.ResourceSet, model.ResourceKey, (Locale)model.Locale);

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<string>(file));

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene el texto de un recurso en formato de archivo
        /// </summary>
        [HttpPost]
        [ActionName("textresource")]
        [Authorize]
        public async Task<HttpResponseMessage> GetTextResource([NakedBody]string jsonModel/*int*/)
        {
            HttpResponseMessage response = null;
            try
            {
                ForResourceDTO model = jsonModel.Decrypt().JsonParse<ForResourceDTO>();

                var file = await _manager.GetTextResource(model.ResourceSet, model.ResourceKey, (Locale)model.Locale);

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<string>(file));

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

        /// <summary>
        /// Obtiene una lista de recursos
        /// </summary>
        [HttpPost]
        [ActionName("textresourceset")]
        [Authorize]
        public async Task<HttpResponseMessage> GetTextResourceSet([NakedBody]string jsonModel/*int*/)
        {
            HttpResponseMessage response = null;
            try
            {
                ForResourceDTO model = jsonModel.Decrypt().JsonParse<ForResourceDTO>();

                var file = await _manager.GetTextResourceSet(model.ResourceSet, (Locale)model.Locale);

                response = Request.CreateResponse(BaseController.CreateResultFromCustomSuccess<IEnumerable<ResourceDTO>>(file));

            }
            catch (Exception e)
            {
                response = Request.CreateResponse(BaseController.CreateResultFromSystemException(e));
            }

            return response;
        }

    }
}

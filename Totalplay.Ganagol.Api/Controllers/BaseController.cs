﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Description;
using System.Web.Http.Metadata;
using Televisa.Trebol.Api.Support.ErrorManagement;
using Televisa.Trebol.Api.Support.Providers;

namespace Televisa.Trebol.Api.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class BaseController 
    {
        protected BaseController()
        {
           
        }

        #region "Creation Logs"


        protected static bool EnableLog
        {
            get
            {
                string enableLog = ConfigurationManager.AppSettings["enableLog"];
                if (!string.IsNullOrEmpty(enableLog))
                {
                    return bool.Parse(enableLog);
                }
                return false;
            }
        }
        protected static bool EnableCompleteLog { get; private set; }

        public static OperationStatus CreateResultFromApplicationException(Exception ex) //where T : IEnumerable, new()
        {
            string lnDateString = DateTime.Now.ToString("yyyyMMddHHmmss");

            OperationStatus opStatus = new OperationStatus
            {
                succesTransaction = false,
                errorMessage = string.Format("Error en aplicación ({0})", lnDateString), // ex.Message,
                statusCode = (int)HttpStatusCode.BadRequest,
                data = "{}".Encrypt(),
            };
            CreateWriteLog(ex, lnDateString);
            return opStatus;
        }

        public static OperationStatus CreateResultFromSystemException(Exception ex) //where T : IEnumerable, new()
        {
            string lnDateString = DateTime.Now.ToString("yyyyMMddHHmmss");
            OperationStatus opStatus = new OperationStatus

            {
                succesTransaction = false,
                errorMessage = string.Format("Error en sistema ({0})", lnDateString),  //ex.Message,
                statusCode = (int)HttpStatusCode.BadRequest,
                data = "{}".Encrypt(),
            };

            CreateWriteLog(ex, lnDateString);
            return opStatus;
        }

        public static OperationStatus CreateResultCustomException<T>(T result, Exception ex, HttpStatusCode typeError) //where T : IEnumerable, new()
        {
            string lnDateString = DateTime.Now.ToString("yyyyMMddHHmmss");

            OperationStatus opStatus = new OperationStatus
            {
                succesTransaction = false,
                errorMessage = string.Format("Error en aplicación ({0})", lnDateString),  //ex.Message,
                statusCode = (int)typeError,
                data = JsonConvert.SerializeObject(result, Formatting.None,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                }).Encrypt(),
            };

            CreateWriteLog(ex, lnDateString);

            return opStatus;
        }

        public static OperationStatus CreateResultForValidation(IEnumerable result) //where T : IEnumerable, new()
        {
            string lnDateString = DateTime.Now.ToString("yyyyMMddHHmmss");

            OperationStatus opStatus = new OperationStatus
            {
                succesTransaction = false,
                errorMessage = string.Format("Error de validación ({0})", lnDateString),
                statusCode = (int)HttpStatusCode.NotAcceptable,
                data = "{}".Encrypt(),
                validationErrors = result,
            };

            CreateWriteLog(string.Format("Error de validación ({0})", lnDateString));

            return opStatus;
        }

        public static OperationStatus CreateResultFromBadOperation<T>(T result, string message = "", HttpStatusCode typeError = HttpStatusCode.BadRequest) //where T :IEnumerable,  new()
        {
            OperationStatus opStatus = new OperationStatus
            {
                succesTransaction = false,
                errorMessage = message,
                statusCode = (int)typeError,
                data = JsonConvert.SerializeObject(result, Formatting.None,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                }).Encrypt(),
            };

            CreateWriteLog(message);
            return opStatus;
        }


        public static OperationStatus CreateResultFromCustomSuccess<T>(T result, string message = "", HttpStatusCode typeError =  HttpStatusCode.OK) //where T :IEnumerable,  new()
        {
            OperationStatus opStatus = new OperationStatus
            {
                succesTransaction = true,
                errorMessage = message,
                statusCode = (int)typeError,
                data = JsonConvert.SerializeObject(result, Formatting.None,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                }).Encrypt(),
            };

            //CreateWriteLog(message);
            return opStatus;
        }

        private static void CreateWriteLog(string msg)
        {
            try
            {
                if (EnableLog)
                {
                    List<string> elements = new List<string>();
                    elements.Add(msg);
                    System.IO.File.AppendAllLines(@"c:\log\NegentropyServiceLog.txt", elements.ToArray());
                }
            }
            catch (Exception)
            {
            }
        }

        private static void CreateWriteLog(Exception ex, string code)
        {
            try
            {
                if (EnableLog)
                {
                    List<string> elements = new List<string>();

                    elements.Add("==============================================================");
                    elements.Add(code);
                    elements.Add(GenerateInnersException(ex));
                    elements.Add("==============================================================");

                    System.IO.File.AppendAllLines(@"c:\log\Totalplay.Ganagol.WebApiExceptionLog.txt", elements.ToArray());
                }
            }
            catch (Exception)
            {
            }
        }

        private static string GenerateInnersException(Exception ex)
        {
            if (ex.InnerException != null)
                return ex.Message + " " + GenerateInnersException(ex.InnerException);

            return ex.Message + "";
        }
        #endregion
    }


    /// <summary>
    /// Reads the Request body into a string/byte[] and
    /// assigns it to the parameter bound.
    /// 
    /// Should only be used with a single parameter on
    /// a Web API method using the [NakedBody] attribute
    /// </summary>
    public class NakedBodyParameterBinding : HttpParameterBinding
    {
        public NakedBodyParameterBinding(HttpParameterDescriptor descriptor)
            : base(descriptor)
        {

        }


        public override Task ExecuteBindingAsync(ModelMetadataProvider metadataProvider,
                                                    HttpActionContext actionContext,
                                                    CancellationToken cancellationToken)
        {
            var binding = actionContext
                .ActionDescriptor
                .ActionBinding;

            if (binding.ParameterBindings.Length > 1 ||
                actionContext.Request.Method == HttpMethod.Get)
                return EmptyTask.Start();

            var type = binding
                        .ParameterBindings[0]
                        .Descriptor.ParameterType;

            if (type == typeof(string))
            {
                return actionContext.Request.Content
                        .ReadAsStringAsync()
                        .ContinueWith((task) =>
                        {
                            var stringResult = task.Result;
                            SetValue(actionContext, stringResult);
                        });
            }
            else if (type == typeof(byte[]))
            {
                return actionContext.Request.Content
                    .ReadAsByteArrayAsync()
                    .ContinueWith((task) =>
                    {
                        byte[] result = task.Result;
                        SetValue(actionContext, result);
                    });
            }

            throw new InvalidOperationException("Only string and byte[] are supported for [NakedBody] parameters");
        }

        public override bool WillReadBody
        {
            get
            {
                return true;
            }
        }
    }

    /// <summary>
    /// A do nothing task that can be returned
    /// from functions that require task results
    /// when there's nothing to do.
    /// 
    /// This essentially returns a completed task
    /// with an empty value structure result.
    /// </summary>
    public class EmptyTask
    {
        public static Task Start()
        {
            var taskSource = new TaskCompletionSource<AsyncVoid>();
            taskSource.SetResult(default(AsyncVoid));
            return taskSource.Task as Task;
        }

        private struct AsyncVoid
        {
        }
    }

    /// <summary>
    /// An attribute that captures the entire content body and stores it
    /// into the parameter of type string or byte[].
    /// </summary>
    /// <remarks>
    /// The parameter marked up with this attribute should be the only parameter as it reads the
    /// entire request body and assigns it to that parameter.    
    /// </remarks>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
    public sealed class NakedBodyAttribute : ParameterBindingAttribute
    {
        public override HttpParameterBinding GetBinding(HttpParameterDescriptor parameter)
        {
            if (parameter == null)
                throw new ArgumentException("Invalid parameter");

            return new NakedBodyParameterBinding(parameter);
        }
    }

}

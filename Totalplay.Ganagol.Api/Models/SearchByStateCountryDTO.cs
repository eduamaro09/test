﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Televisa.Trebol.Api.Models
{
    public class SearchByStateCountryDTO
    {
        public string State { get; set; }
        public string Country { get; set; }  
    }
}
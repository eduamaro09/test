﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Televisa.Trebol.Api.Support.ErrorManagement
{

    public class OperationStatus//<T>
    {
        public bool succesTransaction { get; set; }
        public string errorMessage { get; set; }
        public IEnumerable validationErrors { get; set; }   
        public int statusCode { get; set; }
        public string data { get; set; }
    }

    public class Error
    {
        public int status { get; set; }
        public int code { get; set; }
        public string message { get; set; }
        public string developerMessage { get; set; }
        public IEnumerable validationErrors { get; set; }

        public const int ErrorCodeValidation = (int)HttpStatusCode.BadRequest;
        public const int ErrorServer = (int)HttpStatusCode.InternalServerError;

        public Error()
        {
            this.status = (int)HttpStatusCode.InternalServerError;
            this.code = (int)HttpStatusCode.InternalServerError;
        }

        public Error(IEnumerable validationErrors)
        {
            this.validationErrors = validationErrors;
            this.status = (int)HttpStatusCode.BadRequest;
            this.code = ErrorCodeValidation;
            this.message = "Revisa los campos";
            this.developerMessage = "Error de validación";
        }

        public Error(Exception serverError)
        {
            this.status = (int)HttpStatusCode.InternalServerError;
            this.code = (int)HttpStatusCode.InternalServerError;
            this.message = "Ha ocurrido un error inesperado";
            this.developerMessage = serverError.ToString();
            validationErrors = null;
        }
    }
}